﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Yugamiru
{
    public class DualSourceTextReader
    {
        int m_iAllocate;
        Char[] m_pchText;
        int m_uiSize;
        int m_uiOffset;

        public DualSourceTextReader()
        {
            m_iAllocate = 0;

            m_pchText = null;

            m_uiSize = 0;

            m_uiOffset = 0;

        }




        public DualSourceTextReader(DualSourceTextReader rSrc)
        {
            m_iAllocate = 0;

            m_pchText = null;

            m_uiSize = 0;

            m_uiOffset = 0;

            if (rSrc.m_iAllocate > 0)
            {
                if (rSrc.m_uiSize > 0)
                {
                    m_pchText = new char[rSrc.m_uiSize];
                    if (m_pchText != null)
                    {
                        m_iAllocate = 1;
                        m_pchText = rSrc.m_pchText;
                        m_uiSize = rSrc.m_uiSize;
                        m_uiOffset = rSrc.m_uiOffset;
                    }
                    else
                    {
                        m_iAllocate = 0;
                        m_pchText = null;
                        m_uiSize = 0;
                        m_uiOffset = 0;
                    }
                }
                else
                {
                    m_iAllocate = 0;
                    m_pchText = null;
                    m_uiSize = 0;
                    m_uiOffset = 0;
                }
            }
            else
            {
                m_iAllocate = rSrc.m_iAllocate;
                m_pchText = rSrc.m_pchText;
                m_uiSize = rSrc.m_uiSize;
                m_uiOffset = rSrc.m_uiOffset;
            }
        }

        ~DualSourceTextReader()
        {

            Destruct();
        }

        public void Destruct()
        {
            if (m_iAllocate > 0)
            {
                if (m_pchText != null)
                {
                    //delete[] m_pchText;
                    m_pchText = null;
                }
                m_iAllocate = 0;
            }
            m_uiSize = 0;
            m_uiOffset = 0;
        }
        /*
            CDualSourceTextReader &CDualSourceTextReader::operator=( const CDualSourceTextReader &rSrc )
            {
                Destruct();

                if (rSrc.m_iAllocate)
                {
                    if (rSrc.m_uiSize > 0)
                    {
                        m_pchText = new char[rSrc.m_uiSize];
                        if (m_pchText != NULL)
                        {
                            m_iAllocate = 1;
                            memcpy(m_pchText, rSrc.m_pchText, rSrc.m_uiSize);
                            m_uiSize = rSrc.m_uiSize;
                            m_uiOffset = rSrc.m_uiOffset;
                        }
                        else
                        {
                            m_iAllocate = 0;
                            m_pchText = NULL;
                            m_uiSize = 0;
                            m_uiOffset = 0;
                        }
                    }
                    else
                    {
                        m_iAllocate = 0;
                        m_pchText = NULL;
                        m_uiSize = 0;
                        m_uiOffset = 0;
                    }
                }
                else
                {
                    m_iAllocate = rSrc.m_iAllocate;
                    m_pchText = rSrc.m_pchText;
                    m_uiSize = rSrc.m_uiSize;
                    m_uiOffset = rSrc.m_uiOffset;
                }

                return *this;
            }
        */
        public int Open(string pchFolderName, string pchFileName)
        {
            Destruct();


            /*     HRSRC hResInfo = ::FindResource( ::AfxGetApp()->m_hInstance, pchFileName, "TEXTFILE");
                 if (hResInfo != NULL)
                 {
                     HGLOBAL hResData = ::LoadResource( ::AfxGetApp()->m_hInstance, hResInfo);
                     if (hResData == NULL)
                     {
                         return 0;
                     }
                     m_uiSize = ::SizeofResource(NULL, hResInfo);
                     if (m_uiSize == 0)
                     {
                         return 0;
                     }
                     m_iAllocate = 0;
                     m_pchText = (char*)::LockResource(hResData);
                     return (m_pchText != NULL);
                 }*/
            string strFilePath = pchFolderName;
            strFilePath += pchFileName;

            StreamReader fp = new StreamReader(strFilePath);


            if (fp == null)
            {
                fp.Dispose();
                return 0;
            }


            long lSize = new System.IO.FileInfo(strFilePath).Length;
            if (lSize < 0)
            {
                fp.Close();
                return 0;
            }
            /*  if (fseek(fp, 0L, SEEK_SET) != 0)
              {
                  fclose(fp);
                  fp = NULL;
                  return 0;
              }*/
            m_pchText = new char[lSize];
            if (m_pchText == null)
            {
                fp.Close();
                return 0;
            }
            m_iAllocate = 1;
            m_uiSize = (int)lSize;
            m_uiOffset = 0;
            //while (fp.Peek() >= 0)
            /*  if (fp.Read(m_pchText, 0, (int)lSize) < lSize)
               {
                   fp.Close();
                   fp = null;
                   return 0;
               }*/
            fp.Read(m_pchText, 0, (int)lSize);
            fp.Close();
            fp = null;
            return 1;
        }
        public bool IsEOF()
        {
            //return (m_uiOffset >= m_uiSize);
            if (m_uiOffset >= m_uiSize)
                return true;
            else
                return false;
        }

        public int Read(ref char[] pchBuf, int uiSize)
        {

            int uiIndex = 0;
            while (true)
            {
                if (uiIndex >= uiSize)
                {
                    // o—Íƒoƒbƒtƒ@ƒTƒCƒYƒI[ƒo[.
                    m_uiOffset += uiIndex;
                    break;
                }
                else if (m_uiOffset + uiIndex >= m_uiSize)
                {
                    // “ü—Íƒoƒbƒtƒ@ƒI[ƒo[.
                    pchBuf[uiIndex] = '\0';
                    m_uiOffset += uiIndex;
                    break;
                }

                else if (m_pchText[m_uiOffset + uiIndex] == (0x0D))
                {
                    // ‰üs•¶Žš( CR ).
                    pchBuf[uiIndex] = '\0';
                    //pchBuf = '\0';
                    uiIndex++;
                    if ((m_uiOffset + uiIndex < m_uiSize) && Convert.ToInt64(m_pchText[m_uiOffset + uiIndex]) == 0x0A)
                    {
                        // CR + LF‚È‚ç LF‚à“Ç‚Ý‚Æ‚Î‚·.
                        uiIndex++;
                    }
                    m_uiOffset += uiIndex;
                    break;
                }
                else if (m_pchText[m_uiOffset + uiIndex] == 0x0A)
                {
                    // ‰üs•¶Žš( LF )
                    pchBuf[uiIndex] = '\0';
                    uiIndex++;
                    m_uiOffset += uiIndex;
                    break;
                }
                else
                {
                    char byteSrc = System.Convert.ToChar(m_pchText[m_uiOffset + uiIndex]);
                    if (((0x81 <= byteSrc) && (byteSrc <= 0x9F)) ||
                        ((0xE0 <= byteSrc) && (byteSrc <= 0xFC)))
                    {
                        // 2Byte•¶ŽšƒR[ƒh.
                        if (uiIndex + 1 >= uiSize)
                        {
                            // o—Íƒoƒbƒtƒ@ƒTƒCƒYƒI[ƒo[.
                            pchBuf[m_uiOffset + uiIndex] = '\0';
                            return (uiIndex);

                        }
                        else if (m_uiOffset + uiIndex + 1 >= m_uiSize)
                        {
                            // “ü—Íƒoƒbƒtƒ@ƒI[ƒo[.
                            pchBuf[m_uiOffset + uiIndex] = '\0';
                            return (uiIndex);

                        }
                        else
                        {
                            pchBuf[uiIndex] = System.Convert.ToChar(m_pchText[m_uiOffset + uiIndex]);
                            uiIndex++;
                            pchBuf[uiIndex] = System.Convert.ToChar(m_pchText[m_uiOffset + uiIndex]);
                            uiIndex++;
                        }
                    }
                    else
                    {
                        // 1Byte•¶ŽšƒR[ƒh.
                        pchBuf[uiIndex] = System.Convert.ToChar(m_pchText[m_uiOffset + uiIndex]);
                        uiIndex++;
                    }
                }
            }
            return (uiIndex);
        }



    }
}
