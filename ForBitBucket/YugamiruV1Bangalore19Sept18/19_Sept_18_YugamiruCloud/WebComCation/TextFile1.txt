﻿The below format we can finalize for request and Response :

Request:
[
{
        "Computer_id":"ComputerID/Processer ID from System",
        "Computer_name":"ComputerName",
        "Activation_key":"LicenseKeyValueFromServer"
    }
]


Response:

Message Response Format:

{"LicenseKey":"{0}","ComputerName":"ComputerNameValueFromServer","ComputerID":"ComputerIDValueFromServer","Status":"StatusValueFromServer",
"Message":"MessageValueFromServer","StartDate":"StartDateValueFromServer","EndDate":"EndDateValueFromServer",
"ActivationDate":"ActivationDateValueFromServer",'ComputerKey':'ComputerBoundKeyFromServer','LicenseType':'LicenseTypeValue','KeyStatus':'StatusofLicenseKey'}

Sample Values Additional for understanding:

ComputerKey - V9GC0Y0K0091A13M8J8D341ETXQDY9B

LicenseType - S or M

KeyStatus :'Valid' or  'Invalid

=========================================Current Raw Msg From Server=============================

{"Status":200,"ComputerID":"BFEBFBFF000406E3","ComputerName":"DESKTOP-9QLKVJO","ComputerKey":"V9GC0Y0K0091A13M8J8D341ETXQDY9B","StartDate":"2017-09-19","EndDate":"2019-09-26","ActivationDate":"2017-09-24","License_type":"S","Message":"License Key Activated","Key":"valid"}

=================================================================================================


============================New Response from Server Message Format Should be like===============

{"LicenseKey":"LicenseKeyValueFromServer","ComputerName":"ComputerNameValueFromServer","ComputerID":"ComputerIDValueFromServer","Status":"StatusValueFromServer","Message":"MessageValueFromServer","StartDate":"StartDateValueFromServer","EndDate":"EndDateValueFromServer","ActivationDate":"ActivationDateValueFromServer"}

=================================================================================================