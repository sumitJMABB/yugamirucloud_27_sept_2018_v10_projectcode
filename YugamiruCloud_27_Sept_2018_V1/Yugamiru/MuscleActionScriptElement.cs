﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yugamiru
{
    public class MuscleActionScriptElement
    {
        SymbolFunc m_SymbolFunc = new SymbolFunc();
        public int m_iScriptOpecodeID;     // ƒIƒyƒR[ƒh‚h‚c.
        public int m_iMuscleID;            // ‹Ø“÷‚h‚c.
        public int m_iMuscleStateTypeID;   // ‹Ø“÷ó‘Ôƒ^ƒCƒv‚h‚c.
        public int m_iCollectOperatorID;   // W–ñ‰‰ŽZŽq‚h‚c.
        public int m_iMinValueOperatorID;  // Å¬’l”äŠr‰‰ŽZŽq‚h‚c.
        public int m_iMaxValueOperatorID;  // Å‘å’l”äŠr‰‰ŽZŽq‚h‚c.
        public int m_iMinValue;            // Å¬’l.
        public int m_iMaxValue;            // Å‘å’l.
        public int m_iColoredMuscleID;     // ’…F‹Ø“÷‚h‚c.
        public int m_iMuscleColorID;       // ‹Ø“÷F‚h‚c.
        public string m_strRangeSymbol;       // ”ÍˆÍƒVƒ“ƒ{ƒ‹.
        public MuscleActionScriptElement( )
        {

            m_iScriptOpecodeID = Constants.MUSCLESCRIPTOPECODEID_NOP;	// ƒIƒyƒR[ƒh‚h‚c.

            m_iMuscleID = Constants.MUSCLEID_NONE;                      // ‹Ø“÷‚h‚c.

            m_iMuscleStateTypeID = Constants.MUSCLESTATETYPEID_NONE;    // ‹Ø“÷ó‘Ô‚h‚c.

            m_iCollectOperatorID = Constants.COLLECTOPERATORID_NONE;    // W–ñ‰‰ŽZŽq‚h‚c.

            m_iMinValueOperatorID = Constants.COMPAREOPERATORID_NONE;   // Å¬’l”äŠr‰‰ŽZŽq‚h‚c.

            m_iMaxValueOperatorID = Constants.COMPAREOPERATORID_NONE;   // Å‘å’l”äŠr‰‰ŽZŽq‚h‚c.

            m_iMinValue = 0;                            // Å¬’l.

            m_iMaxValue = 0;                            // Å‘å’l.

            m_iColoredMuscleID = Constants.MUSCLEID_NONE;       // ’…F‹Ø“÷‚h‚c.

            m_iMuscleColorID = Constants.MUSCLECOLORID_NONE;    // ‹Ø“÷F‚h‚c.

            m_strRangeSymbol = "";
        
        }

       
    public void ResolveRange(int iMinValueOperatorID,  int iMaxValueOperatorID,  int iMinValue,  int iMaxValue)
    {
        m_iMinValueOperatorID = iMinValueOperatorID;    // Å¬’l”äŠr‰‰ŽZŽq‚h‚c.
        m_iMaxValueOperatorID = iMaxValueOperatorID;    // Å‘å’l”äŠr‰‰ŽZŽq‚h‚c.
        m_iMinValue = iMinValue;            // Å¬’l.
        m_iMaxValue = iMaxValue;            // Å‘å’l.
    }

    public bool IsSatisfied(double dValue) 
{
	if ( m_iMinValueOperatorID == Constants.COMPAREOPERATORID_GREATER ){
		if ( !( dValue > m_iMinValue ) ){
			return false;
		}	
	}

	if ( m_iMinValueOperatorID == Constants.COMPAREOPERATORID_GREATEROREQUAL)
            {
		if ( !(dValue >= m_iMinValue ) ){
			return false;
		}
	}

	if ( m_iMaxValueOperatorID == Constants.COMPAREOPERATORID_LESS)
            {
		if ( !( dValue<m_iMaxValue ) ){
			return false;
		}	
	}

	if ( m_iMaxValueOperatorID == Constants.COMPAREOPERATORID_LESSOREQUAL)
            {
		if ( !( dValue <= m_iMaxValue ) ){
			return false;
		}	
	}

	return true;
}

public int ReadFromString(ref char[] lpszText)
{
    string strFunctionNameSymbol = string.Empty;

    TextBuffer TextBuffer = new TextBuffer(lpszText);

    TextBuffer.SkipSpaceOrTab();
    if (TextBuffer.IsEOF())
    {
        return (Constants.READFROMSTRING_NULLLINE);
    }

    if (TextBuffer.ReadNewLine())
    {
        if (TextBuffer.IsEOF())
        {
            return (Constants.READFROMSTRING_NULLLINE);
        }
    }

    if (!TextBuffer.ReadSymbol(ref strFunctionNameSymbol))
    {
        return (Constants.READFROMSTRING_SYNTAX_ERROR);
    }

    TextBuffer.SkipSpaceOrTab();
    if (!TextBuffer.ReadLeftParenthesis())
    {
        return (Constants.READFROMSTRING_SYNTAX_ERROR);
    }
    if (strFunctionNameSymbol == "StartMuscleCondition")
    {
        TextBuffer.SkipSpaceOrTab();
        if (!TextBuffer.ReadRightParenthesis())
        {
            return (Constants.READFROMSTRING_SYNTAX_ERROR);
        }
        TextBuffer.SkipSpaceOrTab();
        if (!TextBuffer.ReadSemicolon())
        {
            return (Constants.READFROMSTRING_SYNTAX_ERROR);
        }
        m_iScriptOpecodeID = Constants.MUSCLESCRIPTOPECODEID_START;
        return (Constants.READFROMSTRING_SYNTAX_OK);
    }
    else if (strFunctionNameSymbol == "CheckMuscleCondition")
    {
        String strMuscleSymbol = string.Empty;
        String strMuscleStateTypeSymbol = string.Empty;
                String strCollectOperatorSymbol = string.Empty;
                String strRangeSymbol = string.Empty;

                TextBuffer.SkipSpaceOrTab();
        if (!TextBuffer.ReadSymbol(ref strMuscleSymbol))
        {
            return (Constants.READFROMSTRING_SYNTAX_ERROR);
        }

        TextBuffer.SkipSpaceOrTab();
        if (!TextBuffer.ReadComma())
        {
            return (Constants.READFROMSTRING_SYNTAX_ERROR);
        }

        TextBuffer.SkipSpaceOrTab();
        if (!TextBuffer.ReadSymbol(ref strMuscleStateTypeSymbol))
        {
            return (Constants.READFROMSTRING_SYNTAX_ERROR);
        }

        TextBuffer.SkipSpaceOrTab();
        if (!TextBuffer.ReadComma())
        {
            return (Constants.READFROMSTRING_SYNTAX_ERROR);
        }

        TextBuffer.SkipSpaceOrTab();
        if (!TextBuffer.ReadSymbol(ref strCollectOperatorSymbol))
        {
            return (Constants.READFROMSTRING_SYNTAX_ERROR);
        }

        TextBuffer.SkipSpaceOrTab();
        if (!TextBuffer.ReadComma())
        {
            return (Constants.READFROMSTRING_SYNTAX_ERROR);
        }

        TextBuffer.SkipSpaceOrTab();
        if (!TextBuffer.ReadSymbol(ref strRangeSymbol))
        {
            return (Constants.READFROMSTRING_SYNTAX_ERROR);
        }

        TextBuffer.SkipSpaceOrTab();
        if (!TextBuffer.ReadRightParenthesis())
        {
            return (Constants.READFROMSTRING_SYNTAX_ERROR);
        }
        TextBuffer.SkipSpaceOrTab();
        if (!TextBuffer.ReadSemicolon())
        {
            return (Constants.READFROMSTRING_SYNTAX_ERROR);
        }
        m_iScriptOpecodeID = Constants.MUSCLESCRIPTOPECODEID_CHECK;
        m_iMuscleID = m_SymbolFunc.GetMuscleIDBySymbol(strMuscleSymbol);
        if (m_iMuscleID == Constants.MUSCLEID_NONE)
        {
            return (Constants.READFROMSTRING_MUSCLE_INVALID);
        }
        m_iMuscleStateTypeID = m_SymbolFunc.GetMuscleStateTypeIDBySymbol(strMuscleStateTypeSymbol);
        if (m_iMuscleStateTypeID == Constants.MUSCLESTATETYPEID_NONE)
        {
            return (Constants.READFROMSTRING_MUSCLESTATETYPE_INVALID);
        }
        m_iCollectOperatorID = m_SymbolFunc.GetCollectOperatorIDBySymbol(strCollectOperatorSymbol);
        if (m_iCollectOperatorID == Constants.COLLECTOPERATORID_NONE)
        {
            return (Constants.READFROMSTRING_COLLECTOPERATOR_INVALID);
        }
        m_strRangeSymbol = strRangeSymbol;
        return (Constants.READFROMSTRING_SYNTAX_OK);
    }
    else if (strFunctionNameSymbol == "EndMuscleCondition")
    {
        String strMuscleSymbol =string.Empty;
        String strMuscleColorSymbol = string.Empty;

        TextBuffer.SkipSpaceOrTab();
        if (!TextBuffer.ReadSymbol(ref strMuscleSymbol))
        {
            return (Constants.READFROMSTRING_SYNTAX_ERROR);
        }

        TextBuffer.SkipSpaceOrTab();
        if (!TextBuffer.ReadComma())
        {
            return (Constants.READFROMSTRING_SYNTAX_ERROR);
        }

        TextBuffer.SkipSpaceOrTab();
        if (!TextBuffer.ReadSymbol(ref strMuscleColorSymbol))
        {
            return (Constants.READFROMSTRING_SYNTAX_ERROR);
        }

        TextBuffer.SkipSpaceOrTab();
        if (!TextBuffer.ReadRightParenthesis())
        {
            return (Constants.READFROMSTRING_SYNTAX_ERROR);
        }
        TextBuffer.SkipSpaceOrTab();
        if (!TextBuffer.ReadSemicolon())
        {
            return (Constants.READFROMSTRING_SYNTAX_ERROR);
        }
        m_iScriptOpecodeID = Constants.MUSCLESCRIPTOPECODEID_END;
        m_iColoredMuscleID =m_SymbolFunc.GetMuscleIDBySymbol(strMuscleSymbol);
        if (m_iColoredMuscleID == Constants.MUSCLEID_NONE)
        {
            return (Constants.READFROMSTRING_MUSCLE_INVALID);
        }
        m_iMuscleColorID =m_SymbolFunc.GetMuscleColorIDBySymbol(strMuscleColorSymbol);
        if (m_iMuscleColorID == Constants.MUSCLECOLORID_NONE)
        {
            return (Constants.READFROMSTRING_MUSCLECOLOR_INVALID);
        }
        return (Constants.READFROMSTRING_SYNTAX_OK);
    }

    return Constants.READFROMSTRING_OPECODE_INVALID;
}


    }
}
