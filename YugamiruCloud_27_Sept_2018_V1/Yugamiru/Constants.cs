﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace Yugamiru
{
    public static class Constants
    {
        public const int RESET = 1;
        public const int ZOOMOUT = 2;
        public const int ZOOMIN = 3;
        public const int SAVERESULT = 4;
        
        public const int ADULTMUSCLESTATEPATTERNID_NONE = 0;
        public const int MUSCLESCRIPTOPECODEID_NOP = 0;
        public const int MUSCLESCRIPTOPECODEID_START = 1;
        public const int MUSCLESCRIPTOPECODEID_CHECK = 2;
        public const int MUSCLESCRIPTOPECODEID_END = 3;

        public const uint DIB_RGB_COLORS = 0; /* color table in RGBs */
        public const uint DIB_PAL_COLORS = 1; /* color table in palette indices */

        public const uint SRCCOPY = 0x00CC0020;  

        public const int SW_HIDE = 0;
        public const int SW_SHOWNORMAL = 1;
        public const int SW_NORMAL = 1;
        public const int SW_SHOWMINIMIZED = 2;
        public const int SW_SHOWMAXIMIZED = 3;
        public const int SW_MAXIMIZE = 3;
        public const int SW_SHOWNOACTIVATE = 4;
        public const int SW_SHOW = 5;
        public const int SW_MINIMIZE = 6;
        public const int SW_SHOWMINNOACTIVE = 7;
        public const int SW_SHOWNA = 8;
        public const int SW_RESTORE = 9;
        public const int SW_SHOWDEFAULT = 10;
        public const int SW_FORCEMINIMIZE = 11;
        public const int SW_MAX = 11;

        public const int NI_MAXHOST = 1025; /* Max size of a fully-qualified domain name */
        public const int NI_MAXSERV = 32; /* Max size of a service name */
        public const int DEFAULT_PORT_NUMBER = 12345;/*setting default port number*/

        public const int ENCODEMODE_NUMERIC = 0;
        public const int ENCODEMODE_ALPHANUMERIC = 1;
        public const int ENCODEMODE_BINARY = 2;
        public const int ENCODEMODE_KANJI = 3;

        public const int ECCTYPE_M = 0;
        public const int ECCTYPE_L = 1;
        public const int ECCTYPE_H = 2;
        public const int ECCTYPE_Q = 3;

        public const int MAX_TEXT_LENGTH = 1024;
        public const int TRAINING_SELECTCONDITIONID_NONE = 0;
        public const int TRAINING_SELECTCONDITIONID_BAD_SHOULDERBAL = 1;    //SHOULDERBAL‚Ì’l‚ªˆ«‚¢(Œ¨ƒVƒtƒgj
        public const int TRAINING_SELECTCONDITIONID_BAD_HEADCENTER = 2; //HEADCENTER‚Ì’l‚ªˆ«‚¢iŽñƒVƒtƒgj
        public const int TRAINING_SELECTCONDITIONID_BAD_HIP = 3; //HIP‚Ì’l‚ªˆ«‚¢iœ”Õ‰ñùj
        public const int TRAINING_SELECTCONDITIONID_BAD_CENTERBALANCE = 4;//CENTERBALANCE‚Ì’l‚ªˆ«‚¢iœ”ÕƒVƒtƒgj
        public const int TRAINING_SELECTCONDITIONID_XLEGGED = 5;    //X‹r
        public const int TRAINING_SELECTCONDITIONID_OLEGGED = 6; //O‹r
        public const int TRAINING_SELECTCONDITIONID_POSTUREPATTERN_STOOP_AND_BEND_BACKWARD = 7; /* ”L”w{”½‚è˜ */
        public const int TRAINING_SELECTCONDITIONID_POSTUREPATTERN_STOOP = 8;   /* ”L”w */
        public const int TRAINING_SELECTCONDITIONID_POSTUREPATTERN_BEND_BACKWARD = 9; /* ”½‚è˜ */
        public const int TRAINING_SELECTCONDITIONID_POSTUREPATTERN_FLATBACK = 10; /* ƒtƒ‰ƒbƒgƒoƒbƒN */
        public const int TRAINING_SELECTCONDITIONID_POSTUREPATTERN_NORMAL = 11; /* —‘z‚ÌŽp¨ */
        public const int TRAINING_SELECTCONDITIONID_POSTUREPATTERN_FRONTSIDE_UNBALANCED = 12; /* ³–Êƒoƒ‰ƒ“ƒX”ñ‘ÎÌ */

        public const int POSTUREPATTERNID_NONE = 0;
        public const int POSTUREPATTERNID_STOOP_AND_BEND_BACKWARD = 1;  /* ”L”w{”½‚è˜ */
        public const int POSTUREPATTERNID_STOOP = 2;    /* ”L”w */
        public const int POSTUREPATTERNID_BEND_BACKWARD = 3; /* ”½‚è˜ */
        public const int POSTUREPATTERNID_FLATBACK = 4; /* ƒtƒ‰ƒbƒgƒoƒbƒN */
        public const int POSTUREPATTERNID_NORMAL = 5; /* —‘z‚ÌŽp¨ */
        public const int POSTUREPATTERNID_FRONTSIDE_UNBALANCED = 6; /* ³–Êƒoƒ‰ƒ“ƒX”ñ‘ÎÌ */

        public const int TRAININGID_NONE = 0;
        public const int TRAININGID_01 = 1;
        public const int TRAININGID_02 = 2;
        public const int TRAININGID_03 = 3;
        public const int TRAININGID_04 = 4;
        public const int TRAININGID_05 = 5;
        public const int TRAININGID_06 = 6;
        public const int TRAININGID_07 = 7;
        public const int TRAININGID_08 = 8;
        public const int TRAININGID_09 = 9;
        public const int TRAININGID_10 = 10;
        public const int TRAININGID_11 = 11;
        public const int TRAININGID_12 = 12;
        public const int TRAININGID_13 = 13;
        public const int TRAININGID_14 = 14;
        public const int TRAININGID_15 = 15;
        public const int TRAININGID_16 = 16;
        public const int TRAININGID_17 = 17;
        public const int TRAININGID_18 = 18;
        public const int TRAININGID_19 = 19;
        public const int TRAININGID_20 = 20;
        public const int TRAININGID_21 = 21;
        public const int TRAININGID_22 = 22;
        public const int TRAININGID_MAX = 23;


        public const int INPUTMODE_NONE = 0;
        public const int INPUTMODE_NEW = 1; // V‹K“ü—Íƒ‚[ƒh.
        public const int INPUTMODE_MODIFY = 2;  // C³ƒ‚[ƒh.

        // ŒÂlî•ñ‰æ–Êƒ‚[ƒh.
        public const int MEASUREMENTVIEWMODE_NONE = 0;
        public const int MEASUREMENTVIEWMODE_INITIALIZE = 1;
        public const int MEASUREMENTVIEWMODE_RETAIN = 2;

        // ‰æ‘œ“Çž‰æ–Êƒ‚[ƒh.
        public const int MEASUREMENTSTARTVIEWMODE_NONE = 0;
        public const int MEASUREMENTSTARTVIEWMODE_SIDE_STANDING = 1;
        public const int MEASUREMENTSTARTVIEWMODE_FRONT_STANDING = 2;
        public const int MEASUREMENTSTARTVIEWMODE_FRONT_KNEEDOWN = 3;

        // ŠÖßˆÊ’u•ÒW‰æ–Êƒ‚[ƒh.
        public const int JOINTEDITVIEWMODE_NONE = 0;
        public const int JOINTEDITVIEWMODE_ANKLE_AND_HIP = 1;
        public const int JOINTEDITVIEWMODE_KNEE = 2;
        public const int JOINTEDITVIEWMODE_UPPERBODY = 3;
        public const int JOINTEDITVIEWMODE_SIDE = 4;

        // ƒŒƒ|[ƒg‚É•\Ž¦‚³‚ê‚é„§ƒgƒŒ[ƒjƒ“ƒO‚ÌÅ‘å’l.
        public const int DISPLAYED_NEWTRAINING_COUNT_MAX = 4;


        public const int MYIMAGETYPE_STANDING = 0;
        public const int MYIMAGETYPE_KNEEDOWN = 1;
        public const int MYIMAGETYPE_SIDE = 2;

        public const int BODYPOSITIONTYPEID_STANDING = 1;
        public const int BODYPOSITIONTYPEID_KNEEDOWN = 2;
        public const int BODYPOSITIONTYPEID_COMMON = 3;

        public const int READFROMSTRING_SYNTAX_OK = 0;
        public const int READFROMSTRING_NULLLINE = 1;
        public const int READFROMSTRING_SYNTAX_ERROR = 2;
        public const int READFROMSTRING_MINVALUE_COMPAREOPERATOR_INVALID = 3;
        public const int READFROMSTRING_MAXVALUE_COMPAREOPERATOR_INVALID = 4;
        public const int READFROMSTRING_MUSCLESTATEPATTERN_INVALID = 5;
        public const int READFROMSTRING_MUSCLE_INVALID = 6;
        public const int READFROMSTRING_MUSCLESTATETYPE_INVALID = 7;
        public const int READFROMSTRING_OPECODE_INVALID = 8;
        public const int READFROMSTRING_BODYANGLE_INVALID = 9;
        public const int READFROMSTRING_COLLECTOPERATOR_INVALID = 10;
        public const int READFROMSTRING_MUSCLECOLOR_INVALID = 11;
        public const int READFROMSTRING_RANGE_INVALID = 12;
        public const int READFROMSTRING_RESULT_INVALID = 13;
        public const int READFROMSTRING_BODYPOSITIONTYPE_INVALID = 14;
        //public const int POSTUREPATTERNID_NONE = 0;

     /*       public const Color JOINTS_COLOR = Color.FromArgb(255, 0, 0);
        
        public const int INVALID_JOINTS_COLOR = DIB_RGB_COLORS(108, 108, 30);
        */
        public const int FRONTJOINTEDITWNDMODE_NONE = 0;
        public const int FRONTJOINTEDITWNDMODE_ANKLE_AND_HIP = 1;
        public const int FRONTJOINTEDITWNDMODE_KNEE = 2;
        public const int FRONTJOINTEDITWNDMODE_UPPERBODY = 3;

        public const double IMG_SCALE_MIN = 1.0;
        public const double IMG_SCALE_MAX = 4.0;

        public const double MALE = 0;
        public const double FEMALE = 1;

        public const int YOUNG = 0;
        public const int ADULT = 1;
        public const int ELDER = 2;

        public const int LEFT = 1;
        public const int RIGHT = 0;

        public const int STANDING = 0;
        public const int KNEEDOWN = 1;
        public const int SIDE = 2;

        // ƒ}[ƒJ[ƒTƒCƒY
        public const int MARKER_SIZE_S = 10;
        public const int MARKER_SIZE_M = 12;
        public const int MARKER_SIZE_L = 14;

        public const int PS_SOLID = 0;

        public const int RIGHT_HIP = 0;
        public const int LEFT_HIP = 1;
        public const int RIGHT_KNEE = 2;
        public const int LEFT_KNEE = 3;
        public const int RIGHT_ANKLE = 4;
        public const int LEFT_ANKLE = 5;
        public const int BELT1 = 6;
        public const int BELT2 = 7;
        public const int RIGHT_SHOULDER = 8;
        public const int LEFT_SHOULDER = 9;
        public const int RIGHT_EAR = 10;
        public const int LEFT_EAR = 11;
        public const int CHIN = 12;
        public const int MIDDLE_EYEBROWS = 13;
        public const int TALON = 14;
        public const int FOOT_CURVE = 15;

        public const int VER_FIRST_RELEASE = 1000;
        public const int VER_2005_RELEASE = 1100;
        public const int VER_2006Summer_RELEASE = 1300;
        public const int VER_NOW = VER_2006Summer_RELEASE;
        public const int SIDEJOINTEDITWND_POINTID_NONE = 0;
        public const int SIDEJOINTEDITWND_POINTID_EAR = 1;
        public const int SIDEJOINTEDITWND_POINTID_SHOULDER = 2;
        public const int SIDEJOINTEDITWND_POINTID_RIGHTBELT = 3;
        public const int SIDEJOINTEDITWND_POINTID_LEFTBELT = 4;
        public const int SIDEJOINTEDITWND_POINTID_KNEE = 5;
        public const int SIDEJOINTEDITWND_POINTID_ANKLE = 6;
        public const int SIDEJOINTEDITWND_POINTID_BENCHMARK1 = 7;
        public const int SIDEJOINTEDITWND_POINTID_BENCHMARK2 = 8;
        public const int SIDEJOINTEDITWND_POINTID_KNEE_RIGHTBELT = 9;
        public const int SIDEJOINTEDITWND_POINTID_KNEE_LEFTBELT = 10;
        public const int SIDEJOINTEDITWND_POINTID_ANKLE_RIGHTBELT = 11;
        public const int SIDEJOINTEDITWND_POINTID_ANKLE_LEFTBELT = 12;
        public const int SIDEJOINTEDITWND_POINTID_MAX = 13;

        public const int LABELOVERLAYER_X_TYPE_0 = 0;
        public const int LABELOVERLAYER_X_TYPE_1 = 1;

        public const int LABELOVERLAYER_Y_TYPE_0 = 0;
        public const int LABELOVERLAYER_Y_TYPE_1 = 1;
        public const int LABELOVERLAYER_Y_TYPE_2 = 2;
        public const int LABELOVERLAYER_Y_TYPE_3 = 3;
        public const int LABELOVERLAYER_Y_TYPE_4 = 4;
        public const int LABELOVERLAYER_Y_TYPE_5 = 5;
        public const int LABELOVERLAYER_Y_TYPE_6 = 6;
        public const int LABELOVERLAYER_Y_TYPE_7 = 7;
        public const int LABELOVERLAYER_Y_TYPE_8 = 8;
        public const int LABELOVERLAYER_Y_TYPE_9 = 9;
        public const int LABELOVERLAYER_Y_TYPE_10 = 10;
        public const int LABELOVERLAYER_Y_TYPE_11 = 11;

        public const int LABELOVERLAYER_POSITION_00 = 0;
        public const int LABELOVERLAYER_POSITION_01 = 1;
        public const int LABELOVERLAYER_POSITION_02 = 2;
        public const int LABELOVERLAYER_POSITION_03 = 3;
        public const int LABELOVERLAYER_POSITION_04 = 4;
        public const int LABELOVERLAYER_POSITION_05 = 5;
        public const int LABELOVERLAYER_POSITION_06 = 6;
        public const int LABELOVERLAYER_POSITION_07 = 7;
        public const int LABELOVERLAYER_POSITION_08 = 8;
        public const int LABELOVERLAYER_POSITION_09 = 9;
        public const int LABELOVERLAYER_POSITION_10 = 10;
        public const int LABELOVERLAYER_POSITION_11 = 11;
        public const int LABELOVERLAYER_POSITION_12 = 12;
        public const int LABELOVERLAYER_POSITION_13 = 13;
        public const int LABELOVERLAYER_POSITION_14 = 14;
        public const int LABELOVERLAYER_POSITION_15 = 15;
        public const int LABELOVERLAYER_POSITION_16 = 16;
        public const int LABELOVERLAYER_POSITION_17 = 17;
        public const int LABELOVERLAYER_POSITION_18 = 18;
        public const int LABELOVERLAYER_POSITION_19 = 19;
        public const int LABELOVERLAYER_POSITION_20 = 20;
        public const int LABELOVERLAYER_POSITION_21 = 21;
        public const int LABELOVERLAYER_POSITION_22 = 22;
        public const int LABELOVERLAYER_POSITION_23 = 23;
        public const int LABELOVERLAYER_POSITION_MAX = 24;

        public const int RESULTID_NONE = 0;
        public const int RESULTID_RIGHTKNEE		=			1;
        public const int RESULTID_LEFTKNEE = 2;
        public const int RESULTID_HIP = 3;
        public const int RESULTID_CENTERBALANCE = 4;
        public const int RESULTID_SHOULDERBALANCE = 5;
        public const int RESULTID_EARBALANCE = 6;
        public const int RESULTID_BODYCENTER = 7;
        public const int RESULTID_HEADCENTER = 8;
        public const int RESULTID_SIDEBODYBALANCE = 9;
        public const int RESULTID_NECKFORWARDLEANING = 10;
        public const int RESULTID_POSTUREPATTERN_NORMAL = 11;
        public const int RESULTID_POSTUREPATTERN_STOOP_AND_BEND_BACKWARD = 12;
        public const int RESULTID_POSTUREPATTERN_STOOP = 13;
        public const int RESULTID_POSTUREPATTERN_BEND_BACKWARD = 14;
        public const int RESULTID_POSTUREPATTERN_FLATBACK = 15;
        public const int RESULTID_POSTUREPATTERN_FRONTSIDE_UNBALANCED = 16;
        public const int RESULTID_MAX = 17;

        public const int JUDGEMENT_MESSAGEID_PELVIS_NORMAL = 0;
public const int JUDGEMENT_MESSAGEID_PELVIS_ROTATE_RIGHT = 1;
public const int JUDGEMENT_MESSAGEID_PELVIS_ROTATE_LEFT = 2;
public const int JUDGEMENT_MESSAGEID_PELVIS_TILT_RIGHT = 3;
public const int JUDGEMENT_MESSAGEID_PELVIS_TILT_LEFT = 4;
public const int JUDGEMENT_MESSAGEID_BOTH_LEG_NORMAL = 5;
public const int JUDGEMENT_MESSAGEID_BOTH_LEG_BANDLYLEGGED = 6;
public const int JUDGEMENT_MESSAGEID_BOTH_LEG_CROSSLEGGED = 7;
public const int JUDGEMENT_MESSAGEID_RIGHT_LEG_NORMAL = 8;
public const int JUDGEMENT_MESSAGEID_RIGHT_LEG_BANDLYLEGGED = 9;
public const int JUDGEMENT_MESSAGEID_RIGHT_LEG_CROSSLEGGED = 10;
public const int JUDGEMENT_MESSAGEID_LEFT_LEG_NORMAL = 11;
public const int JUDGEMENT_MESSAGEID_LEFT_LEG_BANDLYLEGGED = 12;
public const int JUDGEMENT_MESSAGEID_LEFT_LEG_CROSSLEGGED = 13;
public const int JUDGEMENT_MESSAGEID_SHOULDER_NORMAL = 14;
public const int JUDGEMENT_MESSAGEID_SHOULDER_TILT_RIGHT_LEVEL1 = 15;
public const int JUDGEMENT_MESSAGEID_SHOULDER_TILT_RIGHT_LEVEL2 = 16;
public const int JUDGEMENT_MESSAGEID_SHOULDER_TILT_LEFT_LEVEL1 = 17;
public const int JUDGEMENT_MESSAGEID_SHOULDER_TILT_LEFT_LEVEL2 = 18;
public const int JUDGEMENT_MESSAGEID_NECK_NORMAL = 19;
public const int JUDGEMENT_MESSAGEID_NECK_TILT_RIGHT_LEVEL1 = 20;
public const int JUDGEMENT_MESSAGEID_NECK_TILT_RIGHT_LEVEL2 = 21;
public const int JUDGEMENT_MESSAGEID_NECK_TILT_LEFT_LEVEL1 = 22;
public const int JUDGEMENT_MESSAGEID_NECK_TILT_LEFT_LEVEL2 = 23;
public const int JUDGEMENT_MESSAGEID_MAX = 24;

public const int JUDGEMENT_MESSAGEPARTID_PELVIS = 0;
        public const int JUDGEMENT_MESSAGEPARTID_BOTH_LEG = 1;
        public const int JUDGEMENT_MESSAGEPARTID_RIGHT_LEG = 2;
        public const int JUDGEMENT_MESSAGEPARTID_LEFT_LEG = 3;
        public const int JUDGEMENT_MESSAGEPARTID_SHOULDER = 4;
        public const int JUDGEMENT_MESSAGEPARTID_NECK = 5;
        public const int JUDGEMENT_MESSAGEPARTID_MAX = 6;

        public const int YUGAMIRU_POINT_RANK_NONE = 0;
        public const int YUGAMIRU_POINT_RANK_A = 1;
        public const int YUGAMIRU_POINT_RANK_B = 2;
        public const int YUGAMIRU_POINT_RANK_C = 3;
        public const int YUGAMIRU_POINT_RANK_D = 4;

        public const int MUSCLEID_NONE = 0;

        public const int MUSCLEID_LATISSIMUSDORSI = 1;
public const int MUSCLEID_BICEPSFEMORIS_LEFT = 2;
public const int MUSCLEID_BICEPSFEMORIS_RIGHT = 3;
public const int MUSCLEID_TENSORFASCIAELATAE_LEFT = 4;
public const int MUSCLEID_TENSORFASCIAELATAE_RIGHT = 5;
public const int MUSCLEID_VASTUSLATERALIS_LEFT = 6;
public const int MUSCLEID_VASTUSLATERALIS_RIGHT = 7;
public const int MUSCLEID_QUADRICEPSFEMORIS_LEFT = 8;
public const int MUSCLEID_QUADRICEPSFEMORIS_RIGHT = 9;
public const int MUSCLEID_HIPJOINTCAPSULE_LEFT = 10;
public const int MUSCLEID_HIPJOINTCAPSULE_RIGHT = 11;
public const int MUSCLEID_GLUTEUSMEDIUS_LEFT = 12;
public const int MUSCLEID_GLUTEUSMEDIUS_RIGHT = 13;
public const int MUSCLEID_RECTUSABDOMINIS = 14;
public const int MUSCLEID_ILIOTIBIALTRACT_LEFT = 15;
public const int MUSCLEID_ILIOTIBIALTRACT_RIGHT = 16;
public const int MUSCLEID_FIBULARIS_LEFT = 17;
public const int MUSCLEID_FIBULARIS_RIGHT = 18;
public const int MUSCLEID_GASTROCNEMIUS_LEFT = 19;
public const int MUSCLEID_GASTROCNEMIUS_RIGHT = 20;
public const int MUSCLEID_CUBOID_LEFT = 21;
public const int MUSCLEID_CUBOID_RIGHT = 22;
public const int MUSCLEID_ADDUCTOR_LEFT = 23;
public const int MUSCLEID_ADDUCTOR_RIGHT = 24;
public const int MUSCLEID_TRAPEZIUS_LEFT = 25;
public const int MUSCLEID_TRAPEZIUS_RIGHT = 26;
public const int MUSCLEID_STERNOCLEIDOMASTOID = 27;
public const int MUSCLEID_MAX = 28;

        public const int MUSCLECOLORID_NONE = 0;
        public const int MUSCLECOLORID_YELLOW = 1;
        public const int MUSCLECOLORID_RED = 2;
        public const int MUSCLECOLORID_BLUE = 3;
        public const int MUSCLECOLORID_MAX = 4;

        public const int MUSCLESTATEPATTERNID_NONE = 0;

        public const int MUSCLESTATETYPEID_NONE = 0;

        public const int COLLECTOPERATORID_NONE = 0;
        public const int COLLECTOPERATORID_GETSUM = 1;
        public const int COLLECTOPERATORID_GETMAX = 2;
        public const int COLLECTOPERATORID_MAX = 3;

        public const int ANGLESCRIPTOPECODEID_NOP = 0;
        public const int ANGLESCRIPTOPECODEID_START = 1;
        public const int ANGLESCRIPTOPECODEID_CHECK = 2;
        public const int ANGLESCRIPTOPECODEID_END = 3;

        public const int BODYPOSITIONTYPEID_NONE = 0;
        public const int BODYANGLEID_NONE = 0;
        public const int COMPAREOPERATORID_NONE = 0;
        public const int COMPAREOPERATORID_TRUE = 1;
        public const int COMPAREOPERATORID_FALSE = 2;
        public const int COMPAREOPERATORID_GREATEROREQUAL = 3;
        public const int COMPAREOPERATORID_GREATER = 4;
        public const int COMPAREOPERATORID_LESSOREQUAL = 5;
        public const int COMPAREOPERATORID_LESS = 6;
        public const int COMPAREOPERATORID_MAX = 7;

        public const int FINAL_SCREEN_MODE_NONE = 0;
        public const int FINAL_SCREEN_MODE_TRUE = 1;

        public const int SETTING_SCREEN_MODE_NONE = 0;
        public const int SETTING_SCREEN_MODE_TRUE = 1;

        public static string[] s = { "\\bin" };

        public static string default_database = Application.StartupPath.Split(s, StringSplitOptions.None)[0] + "\\database\\Yugamiru.sqlite";
        public static string path = Application.StartupPath.Split(s, StringSplitOptions.None)[0] + "\\Scoresheet\\";
        public static string data_path = Application.StartupPath.Split(s, StringSplitOptions.None)[0] + "\\data\\";
        public static string db_file = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) +
             "\\gsport\\Yugamiru cloud\\database\\Yugamiru.sqlite";
        public static string programdata_path = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) +
            "\\gsport\\Yugamiru cloud\\";
        public static   string ms_achFileHeader = "YUGAMIRU";
        public static string ms_achFileVersion = "2000";

        public const int DO_NONE = 0;
        public const int ADD_NEW_RECORD = 1;
        public const int UPDATE_EXISTING_RECORD = 2;



    }
}
