﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Yugamiru
{
    public class SymbolFunc
    {
        public int MUSCLEID_NONE = 0;
        public int MUSCLEID_LATISSIMUSDORSI = 1;
        public int MUSCLEID_BICEPSFEMORIS_LEFT = 2;
        public int MUSCLEID_BICEPSFEMORIS_RIGHT = 3;
        public int MUSCLEID_TENSORFASCIAELATAE_LEFT = 4;
        public int MUSCLEID_TENSORFASCIAELATAE_RIGHT = 5;
        public int MUSCLEID_VASTUSLATERALIS_LEFT = 6;
        public int MUSCLEID_VASTUSLATERALIS_RIGHT = 7;
        public int MUSCLEID_QUADRICEPSFEMORIS_LEFT = 8;
        public int MUSCLEID_QUADRICEPSFEMORIS_RIGHT = 9;
        public int MUSCLEID_HIPJOINTCAPSULE_LEFT = 10;
        public int MUSCLEID_HIPJOINTCAPSULE_RIGHT = 11;
        public int MUSCLEID_GLUTEUSMEDIUS_LEFT = 12;
        public int MUSCLEID_GLUTEUSMEDIUS_RIGHT = 13;
        public int MUSCLEID_RECTUSABDOMINIS = 14;
        public int MUSCLEID_ILIOTIBIALTRACT_LEFT = 15;
        public int MUSCLEID_ILIOTIBIALTRACT_RIGHT = 16;
        public int MUSCLEID_FIBULARIS_LEFT = 17;
        public int MUSCLEID_FIBULARIS_RIGHT = 18;
        public int MUSCLEID_GASTROCNEMIUS_LEFT = 19;
        public int MUSCLEID_GASTROCNEMIUS_RIGHT = 20;
        public int MUSCLEID_CUBOID_LEFT = 21;
        public int MUSCLEID_CUBOID_RIGHT = 22;
        public int MUSCLEID_ADDUCTOR_LEFT = 23;
        public int MUSCLEID_ADDUCTOR_RIGHT = 24;
        public int MUSCLEID_TRAPEZIUS_LEFT = 25;
        public int MUSCLEID_TRAPEZIUS_RIGHT = 26;
        public int MUSCLEID_STERNOCLEIDOMASTOID = 27;
        public int MUSCLEID_MAX = 28;

        // ‹Ø“÷ó‘Ôƒpƒ^[ƒ“‚h‚c‚Ì’è‹`.
        public int MUSCLESTATEPATTERNID_NONE = 0;
        public int MUSCLESTATEPATTERNID_PELVIC_ROTATED_TO_RIGHT = 1;
        public int MUSCLESTATEPATTERNID_PELVIC_ROTATED_TO_LEFT = 2;
        public int MUSCLESTATEPATTERNID_PELVIC_TILTED_TO_RIGHT = 3;
        public int MUSCLESTATEPATTERNID_PELVIC_TILTED_TO_LEFT = 4;
        public int MUSCLESTATEPATTERNID_BANDLY_LEGGED_ON_RIGHT = 5;
        public int MUSCLESTATEPATTERNID_BANDLY_LEGGED_ON_LEFT = 6;
        public int MUSCLESTATEPATTERNID_CROSS_LEGGED_ON_RIGHT = 7;
        public int MUSCLESTATEPATTERNID_CROSS_LEGGED_ON_LEFT = 8;
        public int MUSCLESTATEPATTERNID_SHOULDER_HIGHLY_TILTED_TO_RIGHT = 9;
        public int MUSCLESTATEPATTERNID_SHOULDER_TILTED_TO_RIGHT = 10;
        public int MUSCLESTATEPATTERNID_SHOULDER_TILTED_TO_LEFT = 11;
        public int MUSCLESTATEPATTERNID_SHOULDER_HIGHLY_TILTED_TO_LEFT = 12;
        public int MUSCLESTATEPATTERNID_NECK_HIGHLY_TILTED_TO_RIGHT = 13;
        public int MUSCLESTATEPATTERNID_NECK_TILTED_TO_RIGHT = 14;
        public int MUSCLESTATEPATTERNID_NECK_TILTED_TO_LEFT = 15;
        public int MUSCLESTATEPATTERNID_NECK_HIGHLY_TILTED_TO_LEFT = 16;
        public int MUSCLESTATEPATTERNID_MAX = 17;

        // ‹Ø“÷‚Ì•\Ž¦F‚Ì’è‹`.
        public int MUSCLECOLORID_NONE = 0;
        public int MUSCLECOLORID_YELLOW = 1;
        public int MUSCLECOLORID_RED = 2;
        public int MUSCLECOLORID_BLUE = 3;
        public int MUSCLECOLORID_MAX = 4;

        //‹Ø“÷ó‘Ôƒ^ƒCƒv.
        public int MUSCLESTATETYPEID_NONE = 0;
        public int MUSCLESTATETYPEID_STRAINED = 1;
        public int MUSCLESTATETYPEID_RELAXED = 2;
        public int MUSCLESTATETYPEID_MAX = 3;

        // Šp“xƒXƒNƒŠƒvƒg‚ÌƒIƒyƒR[ƒh‚h‚c.
        public int ANGLESCRIPTOPECODEID_NOP = 0;
        public int ANGLESCRIPTOPECODEID_START = 1;
        public int ANGLESCRIPTOPECODEID_CHECK = 2;
        public int ANGLESCRIPTOPECODEID_END = 3;

        // ‹Ø“÷ƒXƒNƒŠƒvƒg‚ÌƒIƒyƒR[ƒh‚h‚c,
        public int MUSCLESCRIPTOPECODEID_NOP = 0;
        public int MUSCLESCRIPTOPECODEID_START = 1;
        public int MUSCLESCRIPTOPECODEID_CHECK = 2;
        public int MUSCLESCRIPTOPECODEID_END = 3;

        // ”äŠr‰‰ŽZŽq.
        public int COMPAREOPERATORID_NONE = 0;
        public int COMPAREOPERATORID_TRUE = 1;
        public int COMPAREOPERATORID_FALSE = 2;
        public int COMPAREOPERATORID_GREATEROREQUAL = 3;
        public int COMPAREOPERATORID_GREATER = 4;
        public int COMPAREOPERATORID_LESSOREQUAL = 5;
        public int COMPAREOPERATORID_LESS = 6;
        public int COMPAREOPERATORID_MAX = 7;

        //W–ñ‰‰ŽZŽq‚h‚c.
        public int COLLECTOPERATORID_NONE = 0;
        public int COLLECTOPERATORID_GETSUM = 1;
        public int COLLECTOPERATORID_GETMAX = 2;
        public int COLLECTOPERATORID_MAX = 3;

        //@‘ÌˆÊ‚h‚c.
        public int BODYPOSITIONTYPEID_NONE = 0;
        public const int BODYPOSITIONTYPEID_STANDING = 1;
        public const int BODYPOSITIONTYPEID_KNEEDOWN = 2;
        public const int BODYPOSITIONTYPEID_COMMON = 3;
        public int BODYPOSITIONTYPEID_MAX = 4;

        // Šp“x‚h‚c.
        public int BODYANGLEID_NONE = 0;
        public const int BODYANGLEID_HIP = 1;
        public const int BODYANGLEID_CENTER = 2;
        public const int BODYANGLEID_RTHIGH = 3;
        public const int BODYANGLEID_LTHIGH = 4;
        public const int BODYANGLEID_SHOULDER = 5;
        public const int BODYANGLEID_HEAD = 6;
        public const int BODYANGLEID_EAR = 7;
        public const int BODYANGLEID_NECKFORWARDLEANING = 8;
        public const int BODYANGLEID_BODY2LOINS = 9;
        public const int BODYANGLEID_SIDEUPPERCENTER = 10;
        public const int BODYANGLEID_SIDESHOULDEREAR = 11;
        public const int BODYANGLEID_SIDEPELVIS = 12;
        public const int BODYANGLEID_MAX = 13;

        // Œ‹‰Ê‚h‚c.
        public int RESULTID_NONE = 0;
        public int RESULTID_RIGHTKNEE = 1;
        public int RESULTID_LEFTKNEE = 2;
        public int RESULTID_HIP = 3;
        public int RESULTID_CENTERBALANCE = 4;
        public int RESULTID_SHOULDERBALANCE = 5;
        public int RESULTID_EARBALANCE = 6;
        public int RESULTID_BODYCENTER = 7;
        public int RESULTID_HEADCENTER = 8;
        public int RESULTID_SIDEBODYBALANCE = 9;
        public int RESULTID_NECKFORWARDLEANING = 10;
        public int RESULTID_POSTUREPATTERN_NORMAL = 11;
        public int RESULTID_POSTUREPATTERN_STOOP_AND_BEND_BACKWARD = 12;
        public int RESULTID_POSTUREPATTERN_STOOP = 13;
        public int RESULTID_POSTUREPATTERN_BEND_BACKWARD = 14;
        public int RESULTID_POSTUREPATTERN_FLATBACK = 15;
        public int RESULTID_MAX = 16;

        // ƒtƒ@ƒCƒ‹“Ç‚Ýž‚ÝƒGƒ‰[ƒR[ƒh.
        /*public const int READFROMSTRING_SYNTAX_OK = 0;
         public const int READFROMSTRING_NULLLINE = 1;
         public const int READFROMSTRING_SYNTAX_ERROR = 2;
         public const int READFROMSTRING_MINVALUE_COMPAREOPERATOR_INVALID = 3;
         public const int READFROMSTRING_MAXVALUE_COMPAREOPERATOR_INVALID = 4;
         public const int READFROMSTRING_MUSCLESTATEPATTERN_INVALID = 5;
         public const int READFROMSTRING_MUSCLE_INVALID = 6;
         public const int READFROMSTRING_MUSCLESTATETYPE_INVALID = 7;
         public const int READFROMSTRING_OPECODE_INVALID = 8;
         public const int READFROMSTRING_BODYANGLE_INVALID = 9;
         public const int READFROMSTRING_COLLECTOPERATOR_INVALID = 10;
         public const int READFROMSTRING_MUSCLECOLOR_INVALID = 11;
         public const int READFROMSTRING_RANGE_INVALID = 12;
         public const int READFROMSTRING_RESULT_INVALID = 13;
         public const int READFROMSTRING_BODYPOSITIONTYPE_INVALID = 14;*/


        public int GetMuscleIDBySymbol( string pchMuscleSymbol )
        {
            CSymbolAndIDPair[] aSymbolAnbdIDPair = new CSymbolAndIDPair[100];
            

            aSymbolAnbdIDPair[0] = new CSymbolAndIDPair { iID = MUSCLEID_LATISSIMUSDORSI, pchSymbol = "LatissimusDorsi" };
            aSymbolAnbdIDPair[1] = new CSymbolAndIDPair { iID = MUSCLEID_BICEPSFEMORIS_LEFT, pchSymbol = "BicepsFemorisLeft" };
            aSymbolAnbdIDPair[2] = new CSymbolAndIDPair { iID = MUSCLEID_BICEPSFEMORIS_RIGHT, pchSymbol = "BicepsFemorisRight" };
            aSymbolAnbdIDPair[3] = new CSymbolAndIDPair { iID = MUSCLEID_TENSORFASCIAELATAE_LEFT, pchSymbol = "TensorFasciaeLataeLeft" };
            aSymbolAnbdIDPair[4] = new CSymbolAndIDPair { iID = MUSCLEID_TENSORFASCIAELATAE_RIGHT, pchSymbol = "TensorFasciaeLataeRight" };
            aSymbolAnbdIDPair[5] = new CSymbolAndIDPair { iID = MUSCLEID_VASTUSLATERALIS_LEFT, pchSymbol = "VastusLateralisLeft" };
            aSymbolAnbdIDPair[6] = new CSymbolAndIDPair { iID = MUSCLEID_VASTUSLATERALIS_RIGHT, pchSymbol = "VastusLateralisRight" };
            aSymbolAnbdIDPair[7] = new CSymbolAndIDPair { iID = MUSCLEID_QUADRICEPSFEMORIS_LEFT, pchSymbol = "QuadricepsFemorisLeft" };
            aSymbolAnbdIDPair[8] = new CSymbolAndIDPair { iID = MUSCLEID_QUADRICEPSFEMORIS_RIGHT, pchSymbol = "QuadricepsFemorisRight" };
            aSymbolAnbdIDPair[9] = new CSymbolAndIDPair { iID = MUSCLEID_HIPJOINTCAPSULE_LEFT, pchSymbol = "HipJointCapsuleLeft" };
            aSymbolAnbdIDPair[10] = new CSymbolAndIDPair { iID = MUSCLEID_HIPJOINTCAPSULE_RIGHT, pchSymbol = "HipJointCapsuleRight" };
            aSymbolAnbdIDPair[11] = new CSymbolAndIDPair { iID = MUSCLEID_GLUTEUSMEDIUS_LEFT, pchSymbol = "GluteusMediusLeft" };
            aSymbolAnbdIDPair[12] = new CSymbolAndIDPair { iID = MUSCLEID_GLUTEUSMEDIUS_RIGHT, pchSymbol = "GluteusMediusRight" };
            aSymbolAnbdIDPair[13] = new CSymbolAndIDPair { iID = MUSCLEID_RECTUSABDOMINIS, pchSymbol = "RectusAbdominis" };
            aSymbolAnbdIDPair[14] = new CSymbolAndIDPair { iID = MUSCLEID_ILIOTIBIALTRACT_LEFT, pchSymbol = "IliotibialTractLeft" };
            aSymbolAnbdIDPair[15] = new CSymbolAndIDPair { iID = MUSCLEID_ILIOTIBIALTRACT_RIGHT, pchSymbol = "IliotibialTractRight" };
            aSymbolAnbdIDPair[16] = new CSymbolAndIDPair { iID = MUSCLEID_FIBULARIS_LEFT, pchSymbol = "FibularisLeft" };
            aSymbolAnbdIDPair[17] = new CSymbolAndIDPair { iID = MUSCLEID_FIBULARIS_RIGHT, pchSymbol = "FibularisRight" };
            aSymbolAnbdIDPair[18] = new CSymbolAndIDPair { iID = MUSCLEID_GASTROCNEMIUS_LEFT, pchSymbol = "GastrocnemiusLeft" };
            aSymbolAnbdIDPair[19] = new CSymbolAndIDPair { iID = MUSCLEID_GASTROCNEMIUS_RIGHT, pchSymbol = "GastrocnemiusRight" };
            aSymbolAnbdIDPair[20] = new CSymbolAndIDPair { iID = MUSCLEID_CUBOID_LEFT, pchSymbol = "CuboidLeft" };
            aSymbolAnbdIDPair[21] = new CSymbolAndIDPair { iID = MUSCLEID_CUBOID_RIGHT, pchSymbol = "CuboidRight" };
            aSymbolAnbdIDPair[22] = new CSymbolAndIDPair { iID = MUSCLEID_ADDUCTOR_LEFT, pchSymbol = "AdductorLeft" };
            aSymbolAnbdIDPair[23] = new CSymbolAndIDPair { iID = MUSCLEID_ADDUCTOR_RIGHT, pchSymbol = "AdductorRight" };
            aSymbolAnbdIDPair[24] = new CSymbolAndIDPair { iID = MUSCLEID_TRAPEZIUS_LEFT, pchSymbol = "TrapeziusLeft" };
            aSymbolAnbdIDPair[25] = new CSymbolAndIDPair { iID = MUSCLEID_TRAPEZIUS_RIGHT, pchSymbol = "TrapeziusRight" };
            aSymbolAnbdIDPair[26] = new CSymbolAndIDPair { iID = MUSCLEID_STERNOCLEIDOMASTOID, pchSymbol = "Sternocleidomastoid" };
            aSymbolAnbdIDPair[27] = new CSymbolAndIDPair { iID = MUSCLEID_MAX, pchSymbol = "" };
        

                int i = 0;
            for( i = 0; aSymbolAnbdIDPair[i].iID != MUSCLEID_MAX; i++ ){
                if ( aSymbolAnbdIDPair[i].pchSymbol == pchMuscleSymbol  ){
                    return aSymbolAnbdIDPair[i].iID;
                }
        }
            return MUSCLEID_NONE;
        }
        
    /*    public Image GetStandingMuscleBMPFileNameByID(int iMuscleID )
        {
            CSymbolAndIDImage[] aSymbolAnbdIDPair = new CSymbolAndIDImage[100];


            aSymbolAnbdIDPair[0] = new CSymbolAndIDImage { iID = MUSCLEID_LATISSIMUSDORSI, pchSymbol =Yugamiru.Properties.Resources.r_01};

            aSymbolAnbdIDPair[1] = new CSymbolAndIDImage { iID = MUSCLEID_BICEPSFEMORIS_LEFT, pchSymbol = Yugamiru.Properties.Resources.r_02l};
            aSymbolAnbdIDPair[2] = new CSymbolAndIDImage { iID = MUSCLEID_BICEPSFEMORIS_RIGHT, pchSymbol = Yugamiru.Properties.Resources.r_02r};
            aSymbolAnbdIDPair[3] = new CSymbolAndIDImage { iID = MUSCLEID_TENSORFASCIAELATAE_LEFT, pchSymbol = Yugamiru.Properties.Resources.r_03l };
            aSymbolAnbdIDPair[4] = new CSymbolAndIDImage { iID = MUSCLEID_TENSORFASCIAELATAE_RIGHT, pchSymbol = Yugamiru.Properties.Resources.r_03r };
            aSymbolAnbdIDPair[5] = new CSymbolAndIDImage { iID = MUSCLEID_VASTUSLATERALIS_LEFT, pchSymbol = Yugamiru.Properties.Resources.r_04l};
            aSymbolAnbdIDPair[6] = new CSymbolAndIDImage { iID = MUSCLEID_VASTUSLATERALIS_RIGHT, pchSymbol = Yugamiru.Properties.Resources.r_04r };
            aSymbolAnbdIDPair[7] = new CSymbolAndIDImage { iID = MUSCLEID_QUADRICEPSFEMORIS_LEFT, pchSymbol = Yugamiru.Properties.Resources.r_05l };
            aSymbolAnbdIDPair[8] = new CSymbolAndIDImage { iID = MUSCLEID_QUADRICEPSFEMORIS_RIGHT, pchSymbol = Yugamiru.Properties.Resources.r_05r };
            aSymbolAnbdIDPair[9] = new CSymbolAndIDImage { iID = MUSCLEID_HIPJOINTCAPSULE_LEFT, pchSymbol = Yugamiru.Properties.Resources.r_06l };
                aSymbolAnbdIDPair[10] = new CSymbolAndIDImage { iID = MUSCLEID_HIPJOINTCAPSULE_RIGHT, pchSymbol = Yugamiru.Properties.Resources.r_06r };
               aSymbolAnbdIDPair[11] = new CSymbolAndIDImage { iID = MUSCLEID_GLUTEUSMEDIUS_LEFT, pchSymbol = Yugamiru.Properties.Resources.r_07l};
                aSymbolAnbdIDPair[12] = new CSymbolAndIDImage { iID = MUSCLEID_GLUTEUSMEDIUS_RIGHT, pchSymbol = Yugamiru.Properties.Resources.r_07r };
                aSymbolAnbdIDPair[13] = new CSymbolAndIDImage { iID = MUSCLEID_RECTUSABDOMINIS, pchSymbol = Yugamiru.Properties.Resources.r_08};
                aSymbolAnbdIDPair[14] = new CSymbolAndIDImage { iID = MUSCLEID_ILIOTIBIALTRACT_LEFT, pchSymbol = Yugamiru.Properties.Resources.r_09l };
                aSymbolAnbdIDPair[15] = new CSymbolAndIDImage { iID = MUSCLEID_ILIOTIBIALTRACT_RIGHT, pchSymbol = Yugamiru.Properties.Resources.r_09r };
                aSymbolAnbdIDPair[16] = new CSymbolAndIDImage { iID = MUSCLEID_FIBULARIS_LEFT, pchSymbol = Yugamiru.Properties.Resources.r_10l };
                aSymbolAnbdIDPair[17] = new CSymbolAndIDImage { iID = MUSCLEID_FIBULARIS_RIGHT, pchSymbol = Yugamiru.Properties.Resources.r_10r };
                aSymbolAnbdIDPair[18] = new CSymbolAndIDImage { iID = MUSCLEID_GASTROCNEMIUS_LEFT, pchSymbol = Yugamiru.Properties.Resources.r_11l };
                aSymbolAnbdIDPair[19] = new CSymbolAndIDImage { iID = MUSCLEID_GASTROCNEMIUS_RIGHT, pchSymbol = Yugamiru.Properties.Resources.r_11r };
                aSymbolAnbdIDPair[20] = new CSymbolAndIDImage { iID = MUSCLEID_CUBOID_LEFT, pchSymbol = Yugamiru.Properties.Resources.r_12l };
                aSymbolAnbdIDPair[21] = new CSymbolAndIDImage { iID = MUSCLEID_CUBOID_RIGHT, pchSymbol = Yugamiru.Properties.Resources.r_12r };
                aSymbolAnbdIDPair[22] = new CSymbolAndIDImage { iID = MUSCLEID_ADDUCTOR_LEFT, pchSymbol = Yugamiru.Properties.Resources.r_13l };
                aSymbolAnbdIDPair[23] = new CSymbolAndIDImage { iID = MUSCLEID_ADDUCTOR_RIGHT, pchSymbol = Yugamiru.Properties.Resources.r_13r };
                aSymbolAnbdIDPair[24] = new CSymbolAndIDImage { iID = MUSCLEID_TRAPEZIUS_LEFT, pchSymbol = Yugamiru.Properties.Resources.r_14l };
                aSymbolAnbdIDPair[25] = new CSymbolAndIDImage { iID = MUSCLEID_TRAPEZIUS_RIGHT, pchSymbol = Yugamiru.Properties.Resources.r_14r };
                aSymbolAnbdIDPair[26] = new CSymbolAndIDImage { iID = MUSCLEID_STERNOCLEIDOMASTOID, pchSymbol = Yugamiru.Properties.Resources.r_15 };
                aSymbolAnbdIDPair[27] = new CSymbolAndIDImage { iID = MUSCLEID_MAX, pchSymbol = null };
            

        int i = 0;
            for( i = 0; aSymbolAnbdIDPair[i].iID != MUSCLEID_MAX; i++ ){
                if ( aSymbolAnbdIDPair[i].iID == iMuscleID ){
                    return aSymbolAnbdIDPair[i].pchSymbol;
                }
            }
            return null;
        }
*/
/*
        public Image GetKneedownMuscleBMPFileNameByID(int iMuscleID )
        {
            CSymbolAndIDImage[] aSymbolAnbdIDPair = new CSymbolAndIDImage[100];


            aSymbolAnbdIDPair[0] = new CSymbolAndIDImage { iID = MUSCLEID_LATISSIMUSDORSI,
                pchSymbol = Yugamiru.Properties.Resources.k_01
            };
            aSymbolAnbdIDPair[1] = new CSymbolAndIDImage { iID = MUSCLEID_BICEPSFEMORIS_LEFT,
                pchSymbol = Yugamiru.Properties.Resources.k_02l };
            aSymbolAnbdIDPair[2] = new CSymbolAndIDImage
            {
                iID = MUSCLEID_BICEPSFEMORIS_RIGHT,
                pchSymbol = Yugamiru.Properties.Resources.k_02r
            };
            aSymbolAnbdIDPair[3] = new CSymbolAndIDImage
            {
                iID = MUSCLEID_TENSORFASCIAELATAE_LEFT,
                pchSymbol = Yugamiru.Properties.Resources.k_03l
            };
            aSymbolAnbdIDPair[4] = new CSymbolAndIDImage
            {
                iID = MUSCLEID_TENSORFASCIAELATAE_RIGHT,
                pchSymbol = Yugamiru.Properties.Resources.k_03r
            };
            aSymbolAnbdIDPair[5] = new CSymbolAndIDImage
            {
                iID = MUSCLEID_VASTUSLATERALIS_LEFT,
                pchSymbol = Yugamiru.Properties.Resources.k_04l
            };
            aSymbolAnbdIDPair[6] = new CSymbolAndIDImage
            {
                iID = MUSCLEID_VASTUSLATERALIS_RIGHT,
                pchSymbol = Yugamiru.Properties.Resources.k_04r
            };
            aSymbolAnbdIDPair[7] = new CSymbolAndIDImage
            {
                iID = MUSCLEID_QUADRICEPSFEMORIS_LEFT,
                pchSymbol = Yugamiru.Properties.Resources.k_05l
            };
            aSymbolAnbdIDPair[8] = new CSymbolAndIDImage
            {
                iID = MUSCLEID_QUADRICEPSFEMORIS_RIGHT,
                pchSymbol = Yugamiru.Properties.Resources.k_05r
            };
            aSymbolAnbdIDPair[9] = new CSymbolAndIDImage
            {
                iID = MUSCLEID_HIPJOINTCAPSULE_LEFT,
                pchSymbol = Yugamiru.Properties.Resources.k_06l
            };
            aSymbolAnbdIDPair[10] = new CSymbolAndIDImage
            {
                iID = MUSCLEID_HIPJOINTCAPSULE_RIGHT,
                pchSymbol = Yugamiru.Properties.Resources.k_06r
            };
            aSymbolAnbdIDPair[11] = new CSymbolAndIDImage
            {
                iID = MUSCLEID_GLUTEUSMEDIUS_LEFT,
                pchSymbol = Yugamiru.Properties.Resources.k_07l
            };
            aSymbolAnbdIDPair[12] = new CSymbolAndIDImage
            {
                iID = MUSCLEID_GLUTEUSMEDIUS_RIGHT,
                pchSymbol = Yugamiru.Properties.Resources.k_07r
            };
            aSymbolAnbdIDPair[13] = new CSymbolAndIDImage
            {
                iID = MUSCLEID_RECTUSABDOMINIS,
                pchSymbol = Yugamiru.Properties.Resources.k_08
            };
            aSymbolAnbdIDPair[14] = new CSymbolAndIDImage
            {
                iID = MUSCLEID_ILIOTIBIALTRACT_LEFT,
                pchSymbol = Yugamiru.Properties.Resources.k_09l
            };
            aSymbolAnbdIDPair[15] = new CSymbolAndIDImage
            {
                iID = MUSCLEID_ILIOTIBIALTRACT_RIGHT,
                pchSymbol = Yugamiru.Properties.Resources.k_09r
            };
            aSymbolAnbdIDPair[16] = new CSymbolAndIDImage
            {
                iID = MUSCLEID_FIBULARIS_LEFT,
                pchSymbol = Yugamiru.Properties.Resources.k_10l
            };
            aSymbolAnbdIDPair[17] = new CSymbolAndIDImage
            {
                iID = MUSCLEID_FIBULARIS_RIGHT,
                pchSymbol = Yugamiru.Properties.Resources.k_10r
            };
            aSymbolAnbdIDPair[18] = new CSymbolAndIDImage
            {
                iID = MUSCLEID_GASTROCNEMIUS_LEFT,
                pchSymbol = Yugamiru.Properties.Resources.k_11l
            };
            aSymbolAnbdIDPair[19] = new CSymbolAndIDImage
            {
                iID = MUSCLEID_GASTROCNEMIUS_RIGHT,
                pchSymbol = Yugamiru.Properties.Resources.k_11r
            };
            aSymbolAnbdIDPair[20] = new CSymbolAndIDImage
            {
                iID = MUSCLEID_CUBOID_LEFT,
                pchSymbol = Yugamiru.Properties.Resources.k_12l
            };
            aSymbolAnbdIDPair[21] = new CSymbolAndIDImage
            {
                iID = MUSCLEID_CUBOID_RIGHT,
                pchSymbol = Yugamiru.Properties.Resources.k_12r
            };
            aSymbolAnbdIDPair[22] = new CSymbolAndIDImage
            {
                iID = MUSCLEID_ADDUCTOR_LEFT,
                pchSymbol = Yugamiru.Properties.Resources.k_13l
            };
            aSymbolAnbdIDPair[23] = new CSymbolAndIDImage
            {
                iID = MUSCLEID_ADDUCTOR_RIGHT,
                pchSymbol = Yugamiru.Properties.Resources.k_13r
            };
            aSymbolAnbdIDPair[24] = new CSymbolAndIDImage
            {
                iID = MUSCLEID_TRAPEZIUS_LEFT,
                pchSymbol = Yugamiru.Properties.Resources.k_14l
            };
            aSymbolAnbdIDPair[25] = new CSymbolAndIDImage
            {
                iID = MUSCLEID_TRAPEZIUS_RIGHT,
                pchSymbol = Yugamiru.Properties.Resources.k_14r
            };
            aSymbolAnbdIDPair[26] = new CSymbolAndIDImage
            {
                iID = MUSCLEID_STERNOCLEIDOMASTOID,
                pchSymbol = Yugamiru.Properties.Resources.k_15
            };
                aSymbolAnbdIDPair[27] = new CSymbolAndIDImage { iID = MUSCLEID_MAX, pchSymbol = null };
           

        int i = 0;
            for( i = 0; aSymbolAnbdIDPair[i].iID != MUSCLEID_MAX; i++ ){
                if ( aSymbolAnbdIDPair[i].iID == iMuscleID ){
                    return aSymbolAnbdIDPair[i].pchSymbol;
                }
            }
            return null;
        }
 */       

        public int GetMuscleStatePatternIDBySymbol(string pchMuscleStatePatternSymbol)
        {
            

            CSymbolAndIDPair[] aSymbolAnbdIDPair = new CSymbolAndIDPair[100];
            aSymbolAnbdIDPair[0] = new CSymbolAndIDPair { iID = MUSCLESTATEPATTERNID_PELVIC_ROTATED_TO_RIGHT, pchSymbol = "PelvicRotatedToRight" };
            aSymbolAnbdIDPair[1] = new CSymbolAndIDPair { iID = MUSCLESTATEPATTERNID_PELVIC_ROTATED_TO_LEFT, pchSymbol = "PelvicRotatedToLeft" };
            aSymbolAnbdIDPair[2] = new CSymbolAndIDPair { iID = MUSCLESTATEPATTERNID_PELVIC_TILTED_TO_RIGHT, pchSymbol = "PelvicTiltedToRight" };
            aSymbolAnbdIDPair[3] = new CSymbolAndIDPair { iID = MUSCLESTATEPATTERNID_PELVIC_TILTED_TO_LEFT, pchSymbol = "PelvicTiltedToLeft" };
            aSymbolAnbdIDPair[4] = new CSymbolAndIDPair { iID = MUSCLESTATEPATTERNID_BANDLY_LEGGED_ON_RIGHT, pchSymbol = "BandlyLeggedOnRight" };
            aSymbolAnbdIDPair[5] = new CSymbolAndIDPair { iID = MUSCLESTATEPATTERNID_BANDLY_LEGGED_ON_LEFT, pchSymbol = "BandlyLeggedOnLeft" };
            aSymbolAnbdIDPair[6] = new CSymbolAndIDPair { iID = MUSCLESTATEPATTERNID_CROSS_LEGGED_ON_RIGHT, pchSymbol = "CrossLeggedOnRight" };
            aSymbolAnbdIDPair[7] = new CSymbolAndIDPair { iID = MUSCLESTATEPATTERNID_CROSS_LEGGED_ON_LEFT, pchSymbol = "CrossLeggedOnLeft" };
            aSymbolAnbdIDPair[8] = new CSymbolAndIDPair { iID = MUSCLESTATEPATTERNID_SHOULDER_HIGHLY_TILTED_TO_RIGHT, pchSymbol = "ShoulderHighlyTiltedToRight" };
            aSymbolAnbdIDPair[9] = new CSymbolAndIDPair { iID = MUSCLESTATEPATTERNID_SHOULDER_TILTED_TO_RIGHT, pchSymbol = "ShoulderTiltedToRight" };
            aSymbolAnbdIDPair[10] = new CSymbolAndIDPair { iID = MUSCLESTATEPATTERNID_SHOULDER_TILTED_TO_LEFT, pchSymbol = "ShoulderTiltedToLeft" };
            aSymbolAnbdIDPair[11] = new CSymbolAndIDPair { iID = MUSCLESTATEPATTERNID_SHOULDER_HIGHLY_TILTED_TO_LEFT, pchSymbol = "ShoulderHighlyTiltedToLeft" };
            aSymbolAnbdIDPair[12] = new CSymbolAndIDPair { iID = MUSCLESTATEPATTERNID_NECK_HIGHLY_TILTED_TO_RIGHT, pchSymbol = "NeckHighlyTiltedToRight" };
            aSymbolAnbdIDPair[13] = new CSymbolAndIDPair { iID = MUSCLESTATEPATTERNID_NECK_TILTED_TO_RIGHT, pchSymbol = "NeckTiltedToRight" };
            aSymbolAnbdIDPair[14] = new CSymbolAndIDPair { iID = MUSCLESTATEPATTERNID_NECK_TILTED_TO_LEFT, pchSymbol = "NeckTiltedToLeft" };
            aSymbolAnbdIDPair[15] = new CSymbolAndIDPair { iID = MUSCLESTATEPATTERNID_NECK_HIGHLY_TILTED_TO_LEFT, pchSymbol = "NeckHighlyTiltedToLeft" };
            aSymbolAnbdIDPair[16] = new CSymbolAndIDPair { iID = MUSCLESTATEPATTERNID_MAX, pchSymbol = "" };


            int i = 0;
            for (i = 0; aSymbolAnbdIDPair[i].iID != MUSCLESTATEPATTERNID_MAX; i++)
            {
                if (aSymbolAnbdIDPair[i].pchSymbol == pchMuscleStatePatternSymbol)
                {
                    return aSymbolAnbdIDPair[i].iID;
                }
            }
            return MUSCLESTATEPATTERNID_NONE;
        }

        
        public int GetMuscleColorIDBySymbol( string pchMuscleColorSymbol)
        {

            CSymbolAndIDPair[] aSymbolAnbdIDPair = new CSymbolAndIDPair[100];
            aSymbolAnbdIDPair[0] = new CSymbolAndIDPair { iID = MUSCLECOLORID_NONE, pchSymbol = "None" };

            aSymbolAnbdIDPair[0] = new CSymbolAndIDPair { iID = MUSCLECOLORID_YELLOW, pchSymbol = "Yellow" };
            aSymbolAnbdIDPair[1] = new CSymbolAndIDPair { iID = MUSCLECOLORID_RED, pchSymbol = "Red" };
            aSymbolAnbdIDPair[2] = new CSymbolAndIDPair { iID = MUSCLECOLORID_BLUE, pchSymbol = "Blue" };
            aSymbolAnbdIDPair[3] = new CSymbolAndIDPair { iID = MUSCLECOLORID_MAX, pchSymbol = "" };
            

        int i = 0;
            for( i = 0; aSymbolAnbdIDPair[i].iID != MUSCLECOLORID_MAX; i++ ){
                if ( aSymbolAnbdIDPair[i].pchSymbol == pchMuscleColorSymbol  ){
                    return aSymbolAnbdIDPair[i].iID;
                }
            }
            return MUSCLECOLORID_NONE;
        }
        
        public int GetCollectOperatorIDBySymbol( string pchCollectOperatorSymbol)
        {


            CSymbolAndIDPair[] aSymbolAnbdIDPair = new CSymbolAndIDPair[100];
            aSymbolAnbdIDPair[0] = new CSymbolAndIDPair { iID = COLLECTOPERATORID_GETSUM, pchSymbol = "Sum" };


            aSymbolAnbdIDPair[1] = new CSymbolAndIDPair { iID = COLLECTOPERATORID_GETMAX, pchSymbol = "Max" };
            aSymbolAnbdIDPair[2] = new CSymbolAndIDPair { iID = COLLECTOPERATORID_MAX, pchSymbol = "" };
            
        int i = 0;
            for( i = 0; aSymbolAnbdIDPair[i].iID != COLLECTOPERATORID_MAX; i++ ){
                if ( aSymbolAnbdIDPair[i].pchSymbol == pchCollectOperatorSymbol  ){
                    return aSymbolAnbdIDPair[i].iID;
                }
            }
            return COLLECTOPERATORID_NONE;
        }
        
        
        public int GetMuscleStateTypeIDBySymbol( string pchMuscleStateTypeSymbol)
        {

          CSymbolAndIDPair[] aSymbolAnbdIDPair = new CSymbolAndIDPair[100];
        aSymbolAnbdIDPair[0] = new CSymbolAndIDPair { iID = MUSCLESTATETYPEID_STRAINED, pchSymbol = "Strained" };
            aSymbolAnbdIDPair[1] = new CSymbolAndIDPair { iID = MUSCLESTATETYPEID_RELAXED, pchSymbol = "Relaxed" };
            aSymbolAnbdIDPair[2] = new CSymbolAndIDPair { iID = MUSCLESTATETYPEID_MAX, pchSymbol = "" };

           

        int i = 0;
            for( i = 0; aSymbolAnbdIDPair[i].iID != MUSCLESTATETYPEID_MAX; i++ ){
                if ( aSymbolAnbdIDPair[i].pchSymbol == pchMuscleStateTypeSymbol  ){
                    return aSymbolAnbdIDPair[i].iID;
                }
            }
            return MUSCLESTATETYPEID_NONE;
        }

        public int GetBodyPositionTypeIDBySymbol(string pchBodyPositionTypeSymbol)
        {

            /*   class CSymbolAndIDPair
           {
               public:
                   int iID;
               const char* pchSymbol;
           };*/
            CSymbolAndIDPair[] aSymbolAnbdIDPair = new CSymbolAndIDPair[100];
            aSymbolAnbdIDPair[0] = new CSymbolAndIDPair { iID = BODYPOSITIONTYPEID_STANDING, pchSymbol = "Standing" };
            aSymbolAnbdIDPair[1] = new CSymbolAndIDPair { iID = BODYPOSITIONTYPEID_KNEEDOWN, pchSymbol = "Kneedown" };
            aSymbolAnbdIDPair[2] = new CSymbolAndIDPair { iID = BODYPOSITIONTYPEID_COMMON, pchSymbol = "Common" };
            aSymbolAnbdIDPair[3] = new CSymbolAndIDPair { iID = BODYPOSITIONTYPEID_MAX, pchSymbol = "null" };




            int i = 0;
            for (i = 0; aSymbolAnbdIDPair[i].iID != BODYPOSITIONTYPEID_MAX; i++)
            {
                if (aSymbolAnbdIDPair[i].pchSymbol == pchBodyPositionTypeSymbol)
                {
                    return aSymbolAnbdIDPair[i].iID;
                }
            }
            return BODYPOSITIONTYPEID_NONE;
        }

        public int GetBodyAngleIDBySymbol(string pchBodyAngleSymbol)
        {

            /*public class CSymbolAndIDPair
                {

                    public int iID;
                    public string pchSymbol;
                };*/

            CSymbolAndIDPair[] aSymbolAnbdIDPair = new CSymbolAndIDPair[100];
            aSymbolAnbdIDPair[0] = new CSymbolAndIDPair { iID = BODYANGLEID_HIP, pchSymbol = "hip" };
            aSymbolAnbdIDPair[1] = new CSymbolAndIDPair { iID = BODYANGLEID_CENTER, pchSymbol = "center" };
            aSymbolAnbdIDPair[2] = new CSymbolAndIDPair { iID = BODYANGLEID_RTHIGH, pchSymbol = "rthigh" };
            aSymbolAnbdIDPair[3] = new CSymbolAndIDPair { iID = BODYANGLEID_LTHIGH, pchSymbol = "lthigh" };
            aSymbolAnbdIDPair[4] = new CSymbolAndIDPair { iID = BODYANGLEID_SHOULDER, pchSymbol = "shoulder" };
            aSymbolAnbdIDPair[5] = new CSymbolAndIDPair { iID = BODYANGLEID_HEAD, pchSymbol = "head" };
            aSymbolAnbdIDPair[6] = new CSymbolAndIDPair { iID = BODYANGLEID_EAR, pchSymbol = "ear" };
            aSymbolAnbdIDPair[7] = new CSymbolAndIDPair { iID = BODYANGLEID_NECKFORWARDLEANING, pchSymbol = "NeckForwardLeaning" };
            aSymbolAnbdIDPair[8] = new CSymbolAndIDPair { iID = BODYANGLEID_BODY2LOINS, pchSymbol = "body2loins" };
            aSymbolAnbdIDPair[9] = new CSymbolAndIDPair { iID = BODYANGLEID_SIDEUPPERCENTER, pchSymbol = "sideuppercenter" };
            aSymbolAnbdIDPair[10] = new CSymbolAndIDPair { iID = BODYANGLEID_SIDESHOULDEREAR, pchSymbol = "sideshoulderear" };
            aSymbolAnbdIDPair[11] = new CSymbolAndIDPair { iID = BODYANGLEID_SIDEPELVIS, pchSymbol = "sidepelvis" };
            aSymbolAnbdIDPair[12] = new CSymbolAndIDPair { iID = BODYANGLEID_MAX, pchSymbol = "NULL" };






            int i = 0;
            for (i = 0; aSymbolAnbdIDPair[i].iID != BODYANGLEID_MAX; i++)
            {
                if (aSymbolAnbdIDPair[i].pchSymbol == pchBodyAngleSymbol)
                {
                    return aSymbolAnbdIDPair[i].iID;
                }
            }
            return BODYANGLEID_NONE;
        }



        public int GetResultIDBySymbol(string pchResultSymbol)
        {

            /* class CSymbolAndIDPair
             {
             public:
                 int iID;
             const char* pchSymbol;
             };*/

            CSymbolAndIDPair[] aSymbolAnbdIDPair = new CSymbolAndIDPair[100];
            aSymbolAnbdIDPair[0] = new CSymbolAndIDPair { iID = RESULTID_RIGHTKNEE, pchSymbol = "RightKnee" };
            aSymbolAnbdIDPair[1] = new CSymbolAndIDPair { iID = RESULTID_LEFTKNEE, pchSymbol = "LeftKnee" };
            aSymbolAnbdIDPair[2] = new CSymbolAndIDPair { iID = RESULTID_HIP, pchSymbol = "Hip" };
            aSymbolAnbdIDPair[3] = new CSymbolAndIDPair { iID = RESULTID_CENTERBALANCE, pchSymbol = "CenterBalance" };
            aSymbolAnbdIDPair[4] = new CSymbolAndIDPair { iID = RESULTID_SHOULDERBALANCE, pchSymbol = "ShoulderBalance" };
            aSymbolAnbdIDPair[5] = new CSymbolAndIDPair { iID = RESULTID_EARBALANCE, pchSymbol = "EarBalance" };
            aSymbolAnbdIDPair[6] = new CSymbolAndIDPair { iID = RESULTID_BODYCENTER, pchSymbol = "BodyCenter" };
            aSymbolAnbdIDPair[7] = new CSymbolAndIDPair { iID = RESULTID_HEADCENTER, pchSymbol = "HeadCenter" };
            aSymbolAnbdIDPair[8] = new CSymbolAndIDPair { iID = RESULTID_SIDEBODYBALANCE, pchSymbol = "SideBodyBalance" };
            aSymbolAnbdIDPair[9] = new CSymbolAndIDPair { iID = RESULTID_NECKFORWARDLEANING, pchSymbol = "NeckForwardLeaning" };
            aSymbolAnbdIDPair[10] = new CSymbolAndIDPair { iID = RESULTID_POSTUREPATTERN_NORMAL, pchSymbol = "POSTUREPATTERN_NORMAL" };
            aSymbolAnbdIDPair[11] = new CSymbolAndIDPair { iID = RESULTID_POSTUREPATTERN_STOOP_AND_BEND_BACKWARD, pchSymbol = "POSTUREPATTERN_STOOP_AND_BEND_BACKWARD" };
            aSymbolAnbdIDPair[12] = new CSymbolAndIDPair { iID = RESULTID_POSTUREPATTERN_STOOP, pchSymbol = "POSTUREPATTERN_STOOP" };
            aSymbolAnbdIDPair[13] = new CSymbolAndIDPair { iID = RESULTID_POSTUREPATTERN_BEND_BACKWARD, pchSymbol = "POSTUREPATTERN_BEND_BACKWARD" };
            aSymbolAnbdIDPair[14] = new CSymbolAndIDPair { iID = RESULTID_POSTUREPATTERN_FLATBACK, pchSymbol = "POSTUREPATTERN_FLATBACK" };
            aSymbolAnbdIDPair[15] = new CSymbolAndIDPair { iID = RESULTID_MAX, pchSymbol = "NULL" };

            int i = 0;
            for (i = 0; aSymbolAnbdIDPair[i].iID != RESULTID_MAX; i++)
            {
                if (aSymbolAnbdIDPair[i].pchSymbol == pchResultSymbol)
                {
                    return aSymbolAnbdIDPair[i].iID;
                }
            }
            return RESULTID_NONE;
        }

        public int GetCompareOperatorIDBySymbol(string pchCompareOperatorSymbol)
        {

            /*      class CSymbolAndIDPair
              {

                      int iID;
                  const char* pchSymbol;
              };*/

            /*   static CSymbolAndIDPair aSymbolAnbdIDPair[] = {
                       {COMPAREOPERATORID_GREATEROREQUAL,  "GreaterOrEqual"},
                       {COMPAREOPERATORID_GREATER,         "Greater"},
                       {COMPAREOPERATORID_LESSOREQUAL,     "LessOrEqual"},
                       {COMPAREOPERATORID_LESS,            "Less"},
                       {COMPAREOPERATORID_MAX,             NULL}
                   };*/

            CSymbolAndIDPair[] aSymbolAnbdIDPair = new CSymbolAndIDPair[100];
            aSymbolAnbdIDPair[0] = new CSymbolAndIDPair { iID = COMPAREOPERATORID_GREATEROREQUAL, pchSymbol = "GreaterOrEqual" };
            aSymbolAnbdIDPair[1] = new CSymbolAndIDPair { iID = COMPAREOPERATORID_GREATER, pchSymbol = "Greater" };
            aSymbolAnbdIDPair[2] = new CSymbolAndIDPair { iID = COMPAREOPERATORID_LESSOREQUAL, pchSymbol = "LessOrEqual" };
            aSymbolAnbdIDPair[3] = new CSymbolAndIDPair { iID = COMPAREOPERATORID_LESS, pchSymbol = "Less" };
            aSymbolAnbdIDPair[4] = new CSymbolAndIDPair { iID = COMPAREOPERATORID_MAX, pchSymbol = null };

            int i = 0;
            for (i = 0; aSymbolAnbdIDPair[i].iID != COMPAREOPERATORID_MAX; i++)
            {
                if (string.Compare(aSymbolAnbdIDPair[i].pchSymbol, pchCompareOperatorSymbol) == 0)
                {
                    return aSymbolAnbdIDPair[i].iID;
                }
            }
            return COMPAREOPERATORID_NONE;
        }

        public double GetBodyAngleValue(FrontBodyAngle Angle, SideBodyAngle AngleSide, int iBodyAngleID)
        {
            double dValue = 0.0;

            switch (iBodyAngleID)
            {
                case BODYANGLEID_HIP:
                    return Angle.GetHip();
                    break;
                case BODYANGLEID_CENTER:
                    return Angle.GetCenter();
                    break;
                case BODYANGLEID_RTHIGH:
                    return Angle.GetRightThigh();
                    break;
                case BODYANGLEID_LTHIGH:
                    return Angle.GetLeftThigh();
                    break;
                case BODYANGLEID_SHOULDER:
                    return Angle.GetShoulder();
                    break;
                case BODYANGLEID_HEAD:
                    return Angle.GetHead();
                    break;
                case BODYANGLEID_EAR:
                    return Angle.GetEar();
                    break;
                case BODYANGLEID_NECKFORWARDLEANING:
                    return AngleSide.GetNeckForwardLeaning();
                    break;
                case BODYANGLEID_BODY2LOINS:
                    return Angle.GetBody2Loins();
                    break;
                case BODYANGLEID_SIDEUPPERCENTER:
                    return AngleSide.GetBodyBalance();
                    break;
                case BODYANGLEID_SIDESHOULDEREAR:
                    return AngleSide.GetShoulderEarAngle();
                    break;
                case BODYANGLEID_SIDEPELVIS:
                    return AngleSide.GetPelvicAngle();
                    break;
                default:
                    break;
            }
            return dValue;
        }

        public void OutputReadErrorString(ref string strError, int iLineNo, int iErrorCode)
        {
            string strNewError = string.Empty;

            switch (iErrorCode)
            {
                case Constants.READFROMSTRING_SYNTAX_OK:
                case Constants.READFROMSTRING_NULLLINE:
                    break;
                case Constants.READFROMSTRING_SYNTAX_ERROR:
                    strNewError = "Line :"+ iLineNo+" SYNTAX_ERROR\n";
                    break;
                case Constants.READFROMSTRING_MINVALUE_COMPAREOPERATOR_INVALID:
                    strNewError = "Line :"+ iLineNo+" MINVALUE_COMPAREOPERATOR_INVALID\n";
                    break;
                case Constants.READFROMSTRING_MAXVALUE_COMPAREOPERATOR_INVALID:
                    strNewError = "Line :"+ iLineNo+" MAXVALUE_COMPAREOPERATOR_INVALID\n";
                    break;
                case Constants.READFROMSTRING_MUSCLESTATEPATTERN_INVALID:
                    strNewError = "Line :" + iLineNo + " MUSCLESTATEPATTERN_INVALID\n";
                    break;
                case Constants.READFROMSTRING_MUSCLE_INVALID:
                    strNewError = "Line :"+ iLineNo+" MUSCLE_INVALID\n";
                    break;
                case Constants.READFROMSTRING_MUSCLESTATETYPE_INVALID:
                    strNewError = "Line :"+ iLineNo+" MUSCLESTATETYPE_INVALID\n";
                    break;
                case Constants.READFROMSTRING_OPECODE_INVALID:
                    strNewError = "Line :"+ iLineNo+" OPECODE_INVALID\n";
                    break;
                case Constants.READFROMSTRING_BODYANGLE_INVALID:
                    strNewError = "Line :"+ iLineNo+" BODYANGLE_INVALID\n";
                    break;
                case Constants.READFROMSTRING_COLLECTOPERATOR_INVALID:
                    strNewError = "Line :"+ iLineNo+" COLLECTOPERATOR_INVALID\n";
                    break;
                case Constants.READFROMSTRING_MUSCLECOLOR_INVALID:
                    strNewError = "Line :"+ iLineNo+" MUSCLECOLOR_INVALID\n";
                    break;
                case Constants.READFROMSTRING_RANGE_INVALID:
                    strNewError = "Line :"+ iLineNo+" RANGE_INVALID\n";
                    break;
                case Constants.READFROMSTRING_RESULT_INVALID:
                    strNewError = "Line :"+ iLineNo+" RESULT_INVALID\n";
                    break;
                case Constants.READFROMSTRING_BODYPOSITIONTYPE_INVALID:
                    strNewError = "Line :"+ iLineNo+" BODYPOSITIONTYPE_INVALID\n";
                    break;
                default:
                    break;
            }
            if (!(string.IsNullOrEmpty(strNewError)))
            {
                strError += strNewError;
            }
        }
        // }


        public class CSymbolAndIDPair
        {

            public int iID;
            public string pchSymbol;
            
        }
        public class CSymbolAndIDImage
        {

            public int iID;
            public Image pchSymbol;
        }
    }
}
