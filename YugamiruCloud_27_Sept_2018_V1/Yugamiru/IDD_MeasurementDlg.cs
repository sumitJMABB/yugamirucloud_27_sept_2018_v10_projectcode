﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using System.Threading;
using System.Windows.Forms;

namespace Yugamiru
{
    public partial class IDD_MeasurementDlg : Form
    {
        PictureBox picturebox1 = new PictureBox();
        Bitmap bmBack1;
        //public Panel Panel_reference;
        JointEditDoc m_GetDocument;

        public IDD_MeasurementDlg(JointEditDoc GetDocument)
        {
            InitializeComponent();
            //--Added by Rajnish For Background License check--//
            objBackgroundWorker.WorkerSupportsCancellation = true;
            objBackgroundWorker.DoWork += ObjBackgroundWorker_DoWork;
            objBackgroundWorker.RunWorkerCompleted += ObjBackgroundWorker_RunWorkerCompleted;
            //------------------------------------------------//
            bmBack1 = Yugamiru.Properties.Resources.Mainpic2;
            this.Controls.Add(picturebox1);

            picturebox1.Size = new Size(bmBack1.Size.Width, bmBack1.Size.Height);
            picturebox1.Image = bmBack1;

            switch (GetDocument.GetInputMode())
            {
                case Constants.INPUTMODE_NEW:
                    {
                        IDC_BackBtn.Image = Yugamiru.Properties.Resources.gobackgreen_up;
                        IDC_BackBtn.Visible = true;
                        IDC_NextBtn.Image = Yugamiru.Properties.Resources.gonextgreen_up;
                        IDC_NextBtn.Visible = true;

                    }
                    break;
                case Constants.INPUTMODE_MODIFY:
                    {
                        //IDC_BackBtn.Image.Dispose();
                        IDC_BackBtn.Image = Yugamiru.Properties.Resources.gobackgreen_down;
                        IDC_BackBtn.Visible = false;

                        //IDC_NextBtn.Image.Dispose();
                        IDC_NextBtn.Image = Yugamiru.Properties.Resources.completegreen_down;
                        IDC_NextBtn.Visible = true;
                    }
                    break;
                default:
                    break;
            }

            m_GetDocument = GetDocument;

            IDC_ID.MaxLength = 10;
            IDC_ID.Focus();
            IDC_Name.MaxLength = 20;

            DateTime tm = new DateTime();
            DateTime.Now.ToString();

            int y = tm.Year;
            IDC_COMBO_GENDER.Items.Add("-");
            if (GetDocument.GetLanguage() == "Japanese")
            {
                IDC_COMBO_GENDER.Items.Add("男");
                IDC_COMBO_GENDER.Items.Add("女");
            }
            if (GetDocument.GetLanguage() == "Chinese")
            {
                IDC_COMBO_GENDER.Items.Add("男");
                IDC_COMBO_GENDER.Items.Add("女");
            }
            if (GetDocument.GetLanguage() == "Korean")
            {
                IDC_COMBO_GENDER.Items.Add("남성");
                IDC_COMBO_GENDER.Items.Add("여성");
            }
            if (GetDocument.GetLanguage() == "English" || GetDocument.GetLanguage() == "Thai")
            {
                IDC_COMBO_GENDER.Items.Add("Male");
                IDC_COMBO_GENDER.Items.Add("Female");
            }


            // •\Ž¦
            switch (GetDocument.GetMeasurementViewMode())
            {
                case Constants.MEASUREMENTVIEWMODE_INITIALIZE:
                    {
                        IDC_COMBO_DAY.SelectedIndex = 0;
                        IDC_COMBO_GENDER.SelectedIndex = 0;
                        IDC_COMBO_MONTH.SelectedIndex = 0;

                    }
                    break;
                case Constants.MEASUREMENTVIEWMODE_RETAIN:
                    {
                        IDC_ID.Text = GetDocument.GetDataID();
                        IDC_Name.Text = GetDocument.GetDataName();
                        string str = "-";
                        if (GetDocument.GetDataGender() == 1) str = "Male";
                        if (GetDocument.GetDataGender() == 2) str = "Female";

                        IDC_COMBO_GENDER.SelectedIndex = IDC_COMBO_GENDER.Items.IndexOf(str);

                        string year = string.Empty, month = string.Empty, day = string.Empty;
                        GetDocument.GetDataDoB(ref year, ref month, ref day);
                        IDC_BirthYear.Value = Convert.ToInt32(year);

                        IDC_COMBO_MONTH.SelectedIndex = IDC_COMBO_MONTH.Items.IndexOf(month);
                        IDC_COMBO_DAY.SelectedIndex = IDC_COMBO_DAY.Items.IndexOf(day);

                        if (GetDocument.GetDataHeight() == 0)
                        {
                            str = "-";  // g’·
                        }
                        else
                        {
                            str = GetDocument.GetDataHeight().ToString();
                        }
                        IDC_Height.Value = Convert.ToInt32(str);
                    }
                    break;
                case Constants.MEASUREMENTVIEWMODE_NONE:
                default:
                    break;
            }

            CheckInputData();

        }
        public void Reload()
        {
            IDC_ID.Focus();
            IDC_ID.Text = m_GetDocument.GetDataID();
            IDC_Name.Text = m_GetDocument.GetDataName();
            string str = "-";
            // if (m_GetDocument.GetDataGender() == 1) str = "Male";
            // if (m_GetDocument.GetDataGender() == 2) str = "Female";

            // IDC_COMBO_GENDER.SelectedIndex = IDC_COMBO_GENDER.Items.IndexOf(str);

            string year = string.Empty, month = string.Empty, day = string.Empty;
            m_GetDocument.GetDataDoB(ref year, ref month, ref day);
            IDC_BirthYear.Value = Convert.ToInt32(year);

            IDC_COMBO_MONTH.SelectedIndex = IDC_COMBO_MONTH.Items.IndexOf(month);
            IDC_COMBO_DAY.SelectedIndex = IDC_COMBO_DAY.Items.IndexOf(day);

            if (m_GetDocument.GetDataHeight() == 0)
            {
                str = "100";  // g’·
            }
            else
            {
                str = m_GetDocument.GetDataHeight().ToString();
            }
            IDC_Height.Value = Convert.ToInt32(str);
        }
        public void DisposeControls()
        {
            this.picturebox1.Image.Dispose();

            this.IDC_BackBtn.Image.Dispose();
            this.IDC_NextBtn.Image.Dispose();

            Yugamiru.Properties.Resources.gobackgreen_up.Dispose();
            Yugamiru.Properties.Resources.gobackgreen_down.Dispose();
            Yugamiru.Properties.Resources.completegreen_down.Dispose();
            Yugamiru.Properties.Resources.Mainpic2.Dispose();


            this.Dispose();
            this.Close();
        }
        public bool CheckInputData()
        {
            if (IDC_BirthYear.Value.ToString() != null)
            {
                IDC_NextBtn.Visible = true;
            }
            else
            {
                IDC_NextBtn.Visible = false;

            }
            //Invalidate();

            return true;
        }

        //--Added by Rajnish For Background License check--//
        bool IsLicExpired { get; set; }
        string sLicStatus { get; set; }
        //bool IsFirstTimeOnly { get; set; }
        bool IsFirstTimeOnly = true;//--Added by Rajnish To Handle GSP-771--//
        public void ExpLicenceVerify(WebComCation.LicenseStatus s)
        {            
            IsLicExpired = false;
            //ReCheck://--Commented by Rajnish for GSP-798 --//
            //IDC_NextBtn.Enabled = true;
            //WebComCation.LicenseStatus s = WebComCation.LicenseValidator.VerifyPreActivated();
            sLicStatus = s.ToString();
            //if (s != WebComCation.LicenseStatus.Unknown)//--Added by Rajnish For GSP-798--//
            {
                if (s != WebComCation.LicenseStatus.Valid)
                {

                    //Thread.CurrentThread.Abort();
                    IsLicExpired = true;

                    if (InvokeRequired)
                    {
                        this.Invoke(new MethodInvoker(delegate
                        {
                            foreach (Control c in this.Controls)
                            {
                                c.Enabled = false;
                            }
                        }));
                    }
                    else
                    {
                        foreach (Control c in this.Controls)
                        {
                            c.Enabled = false;
                        }
                    }
                    if (IsFirstTimeOnly)//--Added by Rajnish To Handle GSP-771--//
                    {
                        IsFirstTimeOnly = false; //--Added by Rajnish To Handle GSP-771--//
                        MessageBox.Show("Activation key  Expired", "gsport");
                        DialogResult deci = MessageBox.Show("Do you want to provide a valid license?", "gsport", MessageBoxButtons.YesNo);

                        if (deci == DialogResult.Yes)
                        {
                            try
                            {
                                WebComCation.LicenseValidator.IsKVFromLaunch = false; //--Added by Rajnish For GSP-782--//
                                WebComCation.KeyValidator kv = new WebComCation.KeyValidator();
                                kv.ShowDialog();
                                //goto ReCheck;//--Commented by Rajnish for GSP-798 --//
                            }
                            finally
                            {

                                //Added by Sumit GSP-782 For closing the animation progress bar------------START
                                //if (IsCallFromBC)
                                {
                                    System.Diagnostics.Process[] allp = System.Diagnostics.Process.GetProcesses();
                                    foreach (System.Diagnostics.Process p in allp)
                                    {
                                        if (p.ProcessName.ToUpper().Contains("yugamiruPGB".ToUpper()))
                                        {
                                            p.CloseMainWindow();
                                        }
                                    }
                                }
                                //Added by Sumit GSP-782 For closing the animation progress bar------------END
                            }
                        }
                        else
                        {
                            //Added by Sumit GSP-782 For closing the animation progress bar------------START
                            //if (IsCallFromBC)
                            {
                                System.Diagnostics.Process[] allp = System.Diagnostics.Process.GetProcesses();
                                foreach (System.Diagnostics.Process p in allp)
                                {
                                    if (p.ProcessName.ToUpper().Contains("yugamiruPGB".ToUpper()))
                                    {
                                        p.CloseMainWindow();
                                    }
                                }
                            }
                            //Added by Sumit GSP-782 For closing the animation progress bar------------END
                            Application.Exit();
                            //if (InvokeRequired)
                            //{
                            //    this.Invoke(new MethodInvoker(delegate
                            //    {
                            //        IDC_NextBtn.Enabled = false;
                            //    }));
                            //}
                            //else
                            //{
                            //    IDC_NextBtn.Enabled = false;
                            //}

                        }
                    }
                }
                else  //--Added by Rajnish On 06 Sep 18 For GSP-782--start--//
                {
                    IsFirstTimeOnly = true;//--Added by Rajnish To Handle GSP-782--//
                    IsLicExpired = false;

                    if (InvokeRequired)
                    {
                        this.Invoke(new MethodInvoker(delegate
                        {
                            foreach (Control c in this.Controls)
                            {
                                c.Enabled = true;
                            }
                        }));
                    }
                    else
                    {
                        foreach (Control c in this.Controls)
                        {
                            c.Enabled = true;
                        }
                    }
                }//----------------------------------------------------end--//
                 //Added by Sumit GSP-782 For closing the animation progress bar------------START
                 //if (IsCallFromBC)
                {
                    System.Diagnostics.Process[] allp = System.Diagnostics.Process.GetProcesses();
                    foreach (System.Diagnostics.Process p in allp)
                    {
                        if (p.ProcessName.ToUpper().Contains("yugamiruPGB".ToUpper()))
                        {
                            p.CloseMainWindow();
                        }
                    }
                }
                //Added by Sumit GSP-782 For closing the animation progress bar------------END
                //--Added by Rajnish For GSP-782--//
                if (WebComCation.LicenseValidator.IsKVFromLaunch == false)
                {
                    IsFirstTimeOnly = true;//--Added by Rajnish To Handle GSP-782--//
                    if (InvokeRequired)
                    {
                        this.Invoke(new MethodInvoker(delegate
                        {
                            foreach (Control c in this.Controls)
                            {
                                c.Enabled = true;
                            }
                        }));
                    }
                    else
                    {
                        foreach (Control c in this.Controls)
                        {
                            c.Enabled = true;
                        }
                    }
                }
                //------------------------------//
            }
            //--Added by Rajnish For GSP-798--//
        }
        //-------------------------------------------------//
        private void IDD_MeasurementDlg_Load(object sender, EventArgs e)
        {
            // Added by sumit GSP-357, 346: --------START
            #region Commented by Sumit on 22_Mar_18, change validation Startegy
            //ReCheck:
            //WebComCation.LicenseStatus s = WebComCation.LicenseValidator.VerifyPreActivated();
            //if (s != WebComCation.LicenseStatus.Valid)
            //{
            //    Application.Exit();
            //    ////DialogResult deci= MessageBox.Show("Do you want to provide a valid license?", "gsport", MessageBoxButtons.YesNo);

            //    ////if (deci == DialogResult.Yes)
            //    //{
            //    //    try
            //    //    {

            //    //        WebComCation.KeyValidator kv = new WebComCation.KeyValidator();
            //    //        kv.ShowDialog();
            //    //        goto ReCheck;
            //    //    }
            //    //    finally
            //    //    {

            //    //    }
            //    //}
            //    ////else
            //    ////  Environment.Exit(0);
            //}

            #endregion
            //End  GSP-357, 346 Added By Sumit---------END
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {

        }

        public void IDD_MeasurementDlg_SizeChanged(object sender, EventArgs e)
        {
            //--to centre the picture box while resizing the form
            picturebox1.Left = (this.ClientSize.Width - picturebox1.Width) / 2;
            picturebox1.Top = (this.ClientSize.Height - picturebox1.Height) / 2 - 12;

            //--end

            this.IDC_BackBtn.Size = new Size(112, 42);
            this.IDC_BackBtn.Location = new Point(350, 600 + picturebox1.Top);

            this.IDC_NextBtn.Size = new Size(112, 42);
            this.IDC_NextBtn.Location = new Point(800, 600 + picturebox1.Top);

            this.IDC_ID.Size = new Size(182, 47);
            this.IDC_ID.Location = new Point(455, 106 + picturebox1.Top);

            this.IDC_Name.Size = new Size(305, 47);
            this.IDC_Name.Location = new Point(455, 176 + picturebox1.Top);

            this.IDC_COMBO_GENDER.Size = new Size(102, 35);
            this.IDC_COMBO_GENDER.Location = new Point(455, 236 + picturebox1.Top);

            this.IDC_BirthYear.Size = new Size(80, 35);//(77, 41);
            this.IDC_BirthYear.Location = new Point(455, 300 + picturebox1.Top);

            this.IDC_COMBO_MONTH.Size = new Size(70, 35);
            this.IDC_COMBO_MONTH.Location = new Point(582, 300 + picturebox1.Top);

            this.IDC_COMBO_DAY.Size = new Size(70, 35);
            this.IDC_COMBO_DAY.Location = new Point(702, 300 + picturebox1.Top);

            this.IDC_Height.Size = new Size(80, 35);
            this.IDC_Height.Location = new Point(455, 364 + picturebox1.Top);



            //IDC_ID.Top = picturebox1.Top + 110;


            IDC_ID.Left = (this.Width / 2 - IDC_ID.Left) + IDC_ID.Width + 50;
            IDC_Name.Left = (this.Width / 2 - IDC_Name.Left) + IDC_Name.Width - 74;
            IDC_COMBO_GENDER.Left = (this.Width / 2 - IDC_COMBO_GENDER.Left) + IDC_COMBO_GENDER.Width + 130;
            IDC_BirthYear.Left = (this.Width / 2 - IDC_BirthYear.Left) + IDC_BirthYear.Width + 150;
            IDC_COMBO_MONTH.Left = IDC_BirthYear.Left + IDC_COMBO_MONTH.Width + 60;
            IDC_COMBO_DAY.Left = IDC_COMBO_MONTH.Left + IDC_COMBO_DAY.Width + 54;
            IDC_Height.Left = this.Width / 2 - IDC_Height.Left + 230;


            IDC_BackBtn.Left = this.Width / 2 - IDC_BackBtn.Left + 20;
            IDC_NextBtn.Left = IDC_BackBtn.Left + IDC_NextBtn.Width + 400 - 20;

        }

        private void IDC_Name_TextChanged(object sender, EventArgs e)
        {

        }
        Timer t = new Timer();
        static string m_ClickMode = "None";
        void timer_click(object sender, EventArgs e)
        {
            switch (m_ClickMode)
            {
                case "RETURN":
                    IDC_BackBtn.Image = Yugamiru.Properties.Resources.gobackgreen_up;
                    break;
                case "NEXT":
                    switch (m_GetDocument.GetInputMode())
                    {
                        case Constants.INPUTMODE_NEW:
                            m_GetDocument.SetMeasurementStartViewMode(Constants.MEASUREMENTSTARTVIEWMODE_SIDE_STANDING);
                            this.Visible = false;
                            m_GetDocument.GetMeasurementStart().Visible = true;
                            m_GetDocument.GetMeasurementStart().RefreshForm();
                            m_GetDocument.GetMeasurementStart().reload();
                            break;
                        case Constants.INPUTMODE_MODIFY:
                            IDC_NextBtn.Image = Yugamiru.Properties.Resources.completegreen_on;
                            break;
                        default:
                            break;
                    }

                    break;
            }
            t.Stop();
        }
        private void IDC_BackBtn_Click(object sender, EventArgs e)
        {
            //--Added by Rajnish For Background License check--//--Added for GSP-798--//
            if (!objBackgroundWorker.IsBusy)
                objBackgroundWorker.RunWorkerAsync(2000);
            {
                //--Added by Rajnish For Background License check--//
                if (sLicStatus == Convert.ToString(WebComCation.LicenseStatus.Valid))
                {
                    if (!IsLicExpired)
                    {
                        //----------------------------//
                        m_ClickMode = "RETURN";
                        IDC_BackBtn.Image = Yugamiru.Properties.Resources.gobackgreen_down;
                        t.Interval = 100;
                        t.Start();
                        t.Tick += new EventHandler(timer_click);

                        DialogResult dialogResult = MessageBox.Show(
                            /*"if return to title view, current data will be lost. return to title view OK?"*/
                            Yugamiru.Properties.Resources.WARNING1, "Yugamiru", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (dialogResult == DialogResult.Yes)
                        {
                            // CloseForm(EventArgs.Empty); // triggerring the event, to send it to form1 which is base form - step3
                            //this.Close();
                            m_GetDocument.SetInputMode(Constants.INPUTMODE_NEW);
                            this.Visible = false;
                            m_GetDocument.SetMeasurementViewMode(Constants.MEASUREMENTVIEWMODE_NONE);
                            m_GetDocument.GetInitialScreen().Visible = true;
                            m_GetDocument.GetInitialScreen().RefreshForms();
                            m_GetDocument.m_SideImageBytes = null;
                            m_GetDocument.m_FrontKneedownImageBytes = null;
                            m_GetDocument.m_FrontStandingImageBytes = null;
                            IDC_ID.Clear();
                            IDC_Name.Clear();
                            IDC_COMBO_GENDER.SelectedIndex = 0;
                            IDC_COMBO_MONTH.SelectedIndex = 0;

                            IDC_COMBO_DAY.SelectedIndex = 0;

                            //this.DisposeControls();

                        }
                        else if (dialogResult == DialogResult.No)
                        {
                            IDC_BackBtn.Image = Yugamiru.Properties.Resources.gobackgreen_on;
                        }
                    }
                }

            }
        }
        public event EventHandler closeForm; // creating event handler - step1
        public void CloseForm(EventArgs e) // defining the event handler  for triggerring/raising the event - step2
        {
            EventHandler eventHandler = closeForm;
            if (eventHandler != null)
            {

                eventHandler(this, e);
            }
        }

        private void IDC_NextBtn_Click(object sender, EventArgs e)
        {
            //--Added by Rajnish For Background License check--//
            if (!objBackgroundWorker.IsBusy)
                objBackgroundWorker.RunWorkerAsync(2000);
            {
                if (sLicStatus == Convert.ToString(WebComCation.LicenseStatus.Valid))
                {
                    if (!IsLicExpired)
                    {
                        //----------------------------//
                        m_ClickMode = "NEXT";
                        t.Interval = 100;
                        t.Start();
                        t.Tick += new EventHandler(timer_click);
                        m_GetDocument.SetMeasurementViewMode(Constants.MEASUREMENTVIEWMODE_NONE);
                        string y = string.Empty, m = string.Empty, d = string.Empty;

                        y = IDC_BirthYear.Value.ToString();
                        m = IDC_COMBO_MONTH.GetItemText(this.IDC_COMBO_MONTH.SelectedItem).ToString();
                        d = IDC_COMBO_DAY.GetItemText(IDC_COMBO_DAY.SelectedItem).ToString();
                        if (!CheckDate(Convert.ToInt32(y), Convert.ToInt32(m), Convert.ToInt32(d)))
                        {
                            t.Stop();
                            MessageBox.Show("birthday is incorrect");
                            return;
                        }

                        m_GetDocument.SetDataID(IDC_ID.Text);
                        m_GetDocument.SetDataName(IDC_Name.Text);

                        string gender = IDC_COMBO_GENDER.GetItemText(this.IDC_COMBO_GENDER.SelectedItem).ToString();


                        if (gender == "-")
                            m_GetDocument.SetDataGender(0);
                        if (gender == "Male" || gender == "男" || gender == "남성")
                            m_GetDocument.SetDataGender(1);
                        if (gender == "Female" || gender == "女" || gender == "여성")
                            m_GetDocument.SetDataGender(2);

                        m_GetDocument.SetDataDoB(y, m, d);    // ¶”NŒŽ“ú
                        float m_Height = (float)IDC_Height.Value;
                        m_GetDocument.SetDataHeight(m_Height);  // g’·	*/
                        if (m_GetDocument.GetInputMode() == Constants.INPUTMODE_NEW)
                            IDC_NextBtn.Image = Yugamiru.Properties.Resources.gonextgreen_down;
                        else
                        {
                            IDC_NextBtn.Image = Yugamiru.Properties.Resources.completegreen_up;
                            this.Visible = false;
                            FuctiontoStartfromMeasurementViewtoResultView(EventArgs.Empty);
                        }
                    }
                }
            }

        }
        bool CheckDate(int y, int m, int d)
        {

            int ActualDays = DateTime.DaysInMonth(y, m);
            if (y < 1900)
                return false;
            if (d > ActualDays)
                return false;


            return true;
            /*
            bool bLeap = false;
            if (y % 400 == 0 ||
                (y % 4 == 0 && y % 100 != 0))
                bLeap = true;

            if (m < 1 || 12 < m) return false;

            if (d < 1) return false;
            switch (m)
            {
                case 1:
                case 3:
                case 5:
                case 7:
                case 8:
                case 10:
                case 12:
                    if (31 < d) return false;
                    break;
                case 4:
                case 6:
                case 9:
                case 11:
                    if (30 < d) return false;
                    break;
                case 2:
                    if (bLeap && 29 < d) return false;
                    if (!bLeap && 28 < d) return false;
                    break;
            }

            return true;*/
        }
        public event EventHandler EventToStartNextScreen; // creating event handler - step1
        public void CloseFormToStartNextScreen(EventArgs e) // defining the event handler  for triggerring/raising the event - step2
        {
            EventHandler eventHandler = EventToStartNextScreen;
            if (eventHandler != null)
            {

                eventHandler(this, e);
            }
        }
        public event EventHandler EventfromMeasurementViewtoResultView; // creating event handler - step1
        public void FuctiontoStartfromMeasurementViewtoResultView(EventArgs e) // defining the event handler  for triggerring/raising the event - step2
        {
            EventHandler eventHandler = EventfromMeasurementViewtoResultView;
            if (eventHandler != null)
            {

                eventHandler(this, e);
            }
        }

        private void IDD_MeasurementDlg_Paint(object sender, PaintEventArgs e)
        {
            //e.Graphics.DrawImage(bmBack1, (this.Width - bmBack1.Width) / 2, 0, bmBack1.Width, bmBack1.Height );
        }

        private void IDC_COMBO_MONTH_KeyDown(object sender, KeyEventArgs e)
        {
            // comboBox is readonly
            e.SuppressKeyPress = true;
        }

        private void IDC_COMBO_DAY_KeyDown(object sender, KeyEventArgs e)
        {
            // comboBox is readonly
            e.SuppressKeyPress = true;

        }
        public void RefreshForm()
        {
            IDC_ID.Focus();

            int Combo_Count = IDC_COMBO_GENDER.Items.Count;
            for (int i = 0; i < Combo_Count; i++)
            {
                IDC_COMBO_GENDER.Items.RemoveAt(0);
            }

            IDC_COMBO_GENDER.Items.Add("-");

            IDC_COMBO_GENDER.Items.Add(Yugamiru.Properties.Resources.MALE);
            IDC_COMBO_GENDER.Items.Add(Yugamiru.Properties.Resources.FEMALE);

            //string gender = IDC_COMBO_GENDER.GetItemText(this.IDC_COMBO_GENDER.SelectedItem).ToString();

            if (m_GetDocument.GetDataGender() == 0)
            {
                IDC_COMBO_GENDER.SelectedIndex = 0;
            }
            else if (m_GetDocument.GetDataGender() == 1)
            {
                IDC_COMBO_GENDER.SelectedIndex = 1;
            }
            else if (m_GetDocument.GetDataGender() == 2)
            {
                IDC_COMBO_GENDER.SelectedIndex = 2;
            }
            picturebox1.Image = Yugamiru.Properties.Resources.Mainpic2;
            m_GetDocument.GetMainScreen().RefreshMenuStrip(false);
            switch (m_GetDocument.GetInputMode())
            {
                case Constants.INPUTMODE_NEW:
                    {
                        m_GetDocument.SetDataDoB("1980", "1", "1");
                        IDC_BackBtn.Image = Yugamiru.Properties.Resources.gobackgreen_up;
                        IDC_BackBtn.Visible = true;
                        IDC_NextBtn.Image = Yugamiru.Properties.Resources.gonextgreen_up;
                        IDC_NextBtn.Visible = true;

                    }
                    break;
                case Constants.INPUTMODE_MODIFY:
                    {
                        if (m_GetDocument.GetFinalScreenMode() == Constants.FINAL_SCREEN_MODE_NONE)
                        {
                            IDC_BackBtn.Image = Yugamiru.Properties.Resources.gobackgreen_up;
                            IDC_BackBtn.Visible = true;
                            IDC_NextBtn.Image = Yugamiru.Properties.Resources.gonextgreen_up;
                            IDC_NextBtn.Visible = true;
                        }
                        else
                        {
                            IDC_BackBtn.Image = Yugamiru.Properties.Resources.gobackgreen_down;
                            IDC_BackBtn.Visible = false;
                            IDC_NextBtn.Image = Yugamiru.Properties.Resources.completegreen_down;
                            IDC_NextBtn.Visible = true;

                        }


                    }
                    break;
                default:
                    break;
            }

        }

        private void IDC_ID_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void IDC_ID_TextChanged(object sender, EventArgs e)
        {


        }

        private void IDD_MeasurementDlg_KeyDown(object sender, KeyEventArgs e)
        {

        }
        //--Added by Rajnish For Background License Check--//
        BackgroundWorker objBackgroundWorker = new BackgroundWorker();


        private void ObjBackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            ExpLicenceVerify((WebComCation.LicenseStatus)Enum.Parse(typeof(WebComCation.LicenseStatus), sLicStatus));
            //Added by Sumit GSP-821 on 25-Sep-2018-----START
           WebComCation.DateTemperingHandler.SavePresentTimeStamp();
            //Added by Sumit GSP-821 on 25-Sep-2018-----END
        }

        private void ObjBackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            WebComCation.LicenseValidator.IsCallFromBC = true;
            WebComCation.LicenseStatus s = WebComCation.LicenseValidator.VerifyPreActivated();
            WebComCation.LicenseValidator.IsCallFromBC = false;
            sLicStatus = s.ToString();
        }

        private void IDD_MeasurementDlg_VisibleChanged(object sender, EventArgs e)
        {

            //Added by Sumit GSP-782 For closing the animation progress bar------------START
            //if (IsCallFromBC)
            {
                System.Diagnostics.Process[] allp = System.Diagnostics.Process.GetProcesses();
                foreach (System.Diagnostics.Process p in allp)
                {
                    if (p.ProcessName.ToUpper().Contains("yugamiruPGB".ToUpper()))
                    {
                        p.CloseMainWindow();
                    }
                }
            }
            //Added by Sumit GSP-782 For closing the animation progress bar------------END


            if (objBackgroundWorker.IsBusy )
            {
                objBackgroundWorker.CancelAsync();
                objBackgroundWorker = new BackgroundWorker();
                objBackgroundWorker.DoWork += ObjBackgroundWorker_DoWork;
                objBackgroundWorker.RunWorkerCompleted += ObjBackgroundWorker_RunWorkerCompleted;
                objBackgroundWorker.WorkerSupportsCancellation = true;
            }
            else if (!objBackgroundWorker.IsBusy)
            {
                objBackgroundWorker.RunWorkerAsync(2000);
            }
        }
        //-----------------------------------------------------//
    }
}
