﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using Microsoft.Win32;

namespace WebComCation
{
    public class Registry_Handler
    {
        //Actual Values
        ServerOutInMsg oneMessage = new ServerOutInMsg();
         string LicenseKey { get; set; }
         string ComputerName { get; set; }
         string ComputerID { get; set; }
         string Status { get; set; }
         string Message { get; set; }
         string StartDate { get; set; }
         string EndDate { get; set; }
         string ActivationDate { get; set; }
        string LastRunDate { get; set; }
        //Actual Values end

        

        /// <summary>
        /// Call this method after setting the values of all elements
        /// True means write success and null means write failed
        /// </summary>
        /// <returns></returns>
        public static bool? SaveToRegistry( ServerOutInMsg serverMsgToStore)
        {

            bool? saveOp = null; //
            try
            {
                RegistryKey key = Registry.CurrentUser.CreateSubKey(@"SOFTWARE\WinRegistry");
                key.SetValue("LicenseKey", Utility.ConvertToEncrypt( serverMsgToStore.LicenseKey));
                key.SetValue("ComputerName", Utility.ConvertToEncrypt(serverMsgToStore.ComputerName));
                key.SetValue("ComputerID", Utility.ConvertToEncrypt(serverMsgToStore.ComputerID));               
                key.SetValue("Status", Utility.ConvertToEncrypt(serverMsgToStore.Status));
                key.SetValue("ServerMessage", Utility.ConvertToEncrypt(serverMsgToStore.ServerMessage));
                key.SetValue("StartDate", Utility.ConvertToEncrypt(serverMsgToStore.StartDate));
                key.SetValue("EndDate", Utility.ConvertToEncrypt(serverMsgToStore.EndDate));
                key.SetValue("ActivationDate", Utility.ConvertToEncrypt(serverMsgToStore.ActivationDate));
                key.SetValue("LastRunDate", Utility.ConvertToEncrypt(Utility.DateTimeToString(DateTime.Now)));

                try
                {
                    key.Close();
                }
                catch(Exception ex)
                {
                    //Added Sumit GSP-775 on 28-Aug-18 START
                    WebComCation.FaultManager.LogIt(ex);
                    //Added Sumit GSP-775 on 28-Aug-18 END
                }
                //Keys stored successfully
                saveOp = true;
            }
            catch(Exception ex)
            {
                //Something wrong happened so operation was not sucessfull.
                System.Windows.Forms.MessageBox.Show(ex.Message+Environment.NewLine + ex.StackTrace);
                saveOp = false;
                //Added Sumit GSP-775 on 28-Aug-18 START
                WebComCation.FaultManager.LogIt(ex);
                //Added Sumit GSP-775 on 28-Aug-18 END
            }
            return saveOp;
        }
        /// <summary>
        /// Call this method at startup and then elements' values from registry will be available.
        /// True means Read success and false means no keys are present yet and null means some exceptioned occured while reading from registry.
        /// </summary>
        /// <returns></returns>
        bool? ReadInitializeFromReg()
        {
            bool? readOp = null;
            try
            {
                RegistryKey key = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\WinRegistry");
                if (key != null)
                {
                    LicenseKey = Utility.ConvertToStringFromEncrypt((key.GetValue("LicenseKey","").ToString()));
                    ComputerName = Utility.ConvertToStringFromEncrypt((key.GetValue("ComputerName","").ToString()));
                    ComputerID = Utility.ConvertToStringFromEncrypt((key.GetValue("ComputerID","").ToString()));
                    Status = Utility.ConvertToStringFromEncrypt((key.GetValue("Status", "").ToString()));
                    Message = Utility.ConvertToStringFromEncrypt((key.GetValue("Message", "").ToString()));
                    StartDate = Utility.ConvertToStringFromEncrypt((key.GetValue("StartDate", "").ToString()));
                    EndDate = Utility.ConvertToStringFromEncrypt((key.GetValue("EndDate", "").ToString()));
                    ActivationDate = Utility.ConvertToStringFromEncrypt((key.GetValue("ActivationDate", "").ToString()));
                    LastRunDate = Utility.ConvertToStringFromEncrypt((key.GetValue("LastRunDate", "").ToString()));

                    readOp = true;          //success
                    try
                    {
                        key.Close();
                    }
                    catch(Exception ex)
                    {
                    }
                }
                else
                {
                    // Form the serverOutInMsg Manual if the registry does not contain data.
                    ServerOutInMsg msgData = new ServerOutInMsg();


                    msgData.LicenseKey = "";
                    msgData.ComputerName = "";
                    msgData.ComputerID = "";
                    msgData.Status = "";
                    msgData.ServerMessage = "";
                    msgData.StartDate = "";
                    msgData.EndDate = "";
                    msgData.ActivationDate = "";
                    msgData.LastRunDate = "";

                    readOp = true;          //success
                    try
                    {
                        key.Close();
                    }
                    catch { }
                    Registry_Handler.SaveToRegistry(msgData);

                    LicenseKey = "";
                    ComputerName = "";
                    ComputerID = "";
                    Status = "";
                    Message = "";
                    StartDate = "";
                    EndDate = "";
                    ActivationDate = "";
                    LastRunDate = "";

                    readOp = true; //Read failed this will happen if the keys are not present yet in registry. Like at very first run.
                }
            }
            catch(Exception ex)
            {
                //some thing went wrong
                System.Windows.Forms.MessageBox.Show(ex.Message + Environment.NewLine + ex.StackTrace);
                readOp = null;
            }
            


            return readOp;
        }
        public ServerOutInMsg GetStoredRegistryMsg()
        {
            if (ReadInitializeFromReg() == true)
            {
                //set oneMessage
                oneMessage.ActivationDate = this.ActivationDate;
                oneMessage.ComputerID = this.ComputerID;
                oneMessage.ComputerName = this.ComputerName;
                oneMessage.EndDate = this.EndDate;
                oneMessage.LicenseKey = this.LicenseKey;
                oneMessage.ServerMessage = this.Message;
                oneMessage.StartDate = this.StartDate;
                oneMessage.Status = this.Status;
                oneMessage.LastRunDate = this.LastRunDate;
            }
            else
            {
                ////set oneMessage
                //oneMessage.ActivationDate = this.ActivationDate;
                //oneMessage.ComputerID = this.ComputerID;
                //oneMessage.ComputerName = this.ComputerName;
                //oneMessage.EndDate = this.EndDate;
                //oneMessage.LicenseKey = this.LicenseKey;
                oneMessage.ServerMessage = "FirstTime";
                //oneMessage.StartDate = this.StartDate;
                //oneMessage.Status = this.Status;
                //oneMessage.LastRunDate = this.LastRunDate;
            }
            return oneMessage;
        }

        public string RegToJsonRawMsgToSend(ServerOutInMsg msg)
        {
            //string jsonQuery = "[{\"computer_id\": \"" + keyRequest.GetComputerID() +
            //                        "\",\"computer_name\": \"" + keyRequest.GetComputerName() +
            //                        "\",\"activation_key\": \"" + txtLicKey.Text + "\"}]";
            string jsonQuery = "[{\"computer_id\": \"" + keyRequest.GetComputerID() +
                                  "\",\"computer_name\": \"" + keyRequest.GetComputerName() +
                                  "\",\"activation_key\": \"" + oneMessage.LicenseKey + "\"}]";

            return "";
        }

        public static DateTime GetLastRunDateFromReg()
        {
            DateTime dtLastRun = new DateTime();
            object hold = new object();
            hold = "20000921125936";
            RegistryKey key = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\WinRegistry");
            if (key != null)
            {              
                try
                {
                    hold = Utility.ConvertToStringFromEncrypt(key.GetValue("LastRunDate", "20000921125936").ToString());
                }
                catch
                {
                    hold = "20000921125936";
                }
                if(hold==null || hold.ToString().Trim().Length==0)
                {
                    hold = "20000921125936";
                }
                dtLastRun = Utility.StringToDateTime(hold.ToString());//key.GetValue("LastRunDate", "20000921125936").ToString());

            }
            else
            {
                dtLastRun = DateTime.Now;
                SaveCurrentRunDateToReg();
            }
            try
            {
                key.Close();
            }
            catch
            { }
            return dtLastRun;

        }
        public static void SaveCurrentRunDateToReg()
        {

            RegistryKey key = Registry.CurrentUser.CreateSubKey(@"SOFTWARE\WinRegistry");
            key.SetValue("LastRunDate", Utility.ConvertToEncrypt(Utility.DateTimeToString(DateTime.Now)));
            try
            {
                key.Close();
            }
            catch
            { }

        }
        public static void SaveActivationKeyToRegistry(string activationKey)
        {
            RegistryKey key = Registry.CurrentUser.CreateSubKey(@"SOFTWARE\WinRegistry");
            key.SetValue("ActivationKey", Utility.ConvertToEncrypt(activationKey));
            try
            {
                key.Close();
            }
            catch
            { }
        }
        public static void SaveComputerKeyToRegistry( string computerKey)
        {
            RegistryKey key = Registry.CurrentUser.CreateSubKey(@"SOFTWARE\WinRegistry");
            key.SetValue("ComputerKey",Utility.ConvertToEncrypt( computerKey));
            try
            {
                key.Close();
            }
            catch
            { }
        }

        public static void ClearActivationKeyFromRegistry()
        {
            RegistryKey key = Registry.CurrentUser.CreateSubKey(@"SOFTWARE\WinRegistry");
            key.SetValue("ActivationKey", "");
            try
            {
                key.Close();
            }
            catch
            { }
        }
        public static void ClearComputerKeyFromRegistry()
        {
            RegistryKey key = Registry.CurrentUser.CreateSubKey(@"SOFTWARE\WinRegistry");
            key.SetValue("ComputerKey", "");
            try
            {
                key.Close();
            }
            catch
            { }
        }

        public static string GetActivationKeyFromReg()
        {
            string actKey = string.Empty;
            RegistryKey key = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\WinRegistry");
            if (key != null)
            {
                try
                {
                    actKey = Utility.ConvertToStringFromEncrypt(key.GetValue("ActivationKey", "").ToString());
                }
                catch
                {

                }
                finally
                {
                    key.Close();
                }
            }

            return actKey;
        }
        public static string GetComputerKeyFromReg()
        {
            string cmpKey = string.Empty;
            RegistryKey key = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\WinRegistry");
            if (key != null)
            {
                try
                {
                    cmpKey = Utility.ConvertToStringFromEncrypt(key.GetValue("ComputerKey", "").ToString());
                }
                catch
                {

                }
                finally
                {
                    key.Close();
                }
            }

            return cmpKey;
        }
    }
}
