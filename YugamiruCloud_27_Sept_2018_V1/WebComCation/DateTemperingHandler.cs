﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Globalization;
using Microsoft.Win32;
using System.IO;
using System.IO.IsolatedStorage;


//File & Class Created by Sumit on 25-September-2018


namespace WebComCation
{
    /// <summary>
    /// This Class contains all functions required for validating the local PC daettime
    /// </summary>
    public static class DateTemperingHandler
    {
        //public static string GetPCTimeStamp

        //Below Method Implemented by sumit on 25-sep-18
        /// <summary>
        /// Reads the present date from international server if internet is availble
        /// </summary>
        /// <returns></returns>
        public static string GetTimeStampFromInternet()
        {
            string datetimeStamp = "";
            if (!Utility.IsInternetConnected())
            {
                return "";
            }
            else
            {

                var myHttpWebRequest = (HttpWebRequest)WebRequest.Create("http://www.microsoft.com");
                var response = myHttpWebRequest.GetResponse();
                string todaysDates = response.Headers["date"];
                response.Close();
                response.Dispose();


                DateTime localDate = DateTime.ParseExact(todaysDates,
                                    "ddd, dd MMM yyyy HH:mm:ss 'GMT'",
                                    CultureInfo.InvariantCulture.DateTimeFormat,
                                    DateTimeStyles.AssumeUniversal);


                string serverStamp = localDate.Year.ToString() + localDate.Month.ToString() + localDate.Day +
                                    localDate.Hour.ToString() + localDate.Minute.ToString() + localDate.Second.ToString();
                string convertedToLocal = localDate.ToString("yyyy MM dd HH:mm:ss");

                datetimeStamp = convertedToLocal.Replace(" ", "").Replace(":", "");
            }
            return datetimeStamp;
        }




        public static bool IsPCDateValidNow()
        {
            bool chkVar = false;

            if(Utility.IsInternetConnected())
            {
                chkVar = IsPCDateValidWithNet();
            }
            else
            {
                chkVar = IsPCDateValidWithoutNet();
            }

            return chkVar;
        }


        //Below Method Implemented by sumit on 25-sep-18
        /// <summary>
        /// Checks if the system date is correct or not, PC must be connected.
        /// </summary>        
        /// <returns></returns>
        public static bool IsPCDateValidWithNet()
        {
            bool isPCDateValid = true;
            //Read current time stamp of PC.            
            string pcStamp = DateTime.Now.ToString("yyyy MM dd HH:mm:ss");
            pcStamp = pcStamp.Replace(" ", "").Replace(":", "");
            //sample 2018 09 30 12 17 53
            string serverStamp = GetTimeStampFromInternet();
            if (serverStamp.Trim() == "")
            {
                throw new Exception(Properties.Resources.PC_NOT_CONNECTED);
            }
            try
            {

                if (pcStamp.Length != serverStamp.Trim().Length)
                {
                    throw new Exception("Date comparison failed");
                }
                else
                {
                    Int64 pcValue = Convert.ToInt64(pcStamp);
                    Int64 serverValue = Convert.ToInt64(serverStamp);

                    if (serverValue - pcValue > 200000)
                    {
                        isPCDateValid = false;
                    }
                    else
                    {
                        isPCDateValid = true;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Date validation failed");
            }



            return isPCDateValid;
        }



        //Below Method Implemented by sumit on 25-sep-18
        /// <summary>
        /// Checks if the system date is correct or not, for license
        /// </summary>
        /// <param name="ql">license object must conatin the last used datetime value</param>
        /// <returns></returns>
        public static bool IsPCDateValidWithoutNet()
        {
            bool isPCDateValid = true;
            string savedStamp = GetSecureAppTimeStamp();
            //Read current time stamp of PC.
            
            string pcStamp = DateTime.Now.ToString("yyyy MM dd HH:mm:ss");
            pcStamp = pcStamp.Replace(" ", "").Replace(":", "");
            //sample 2018 09 30 12 17
            string licenseStamp = savedStamp;
            try
            {

                if (pcStamp.Length != savedStamp.Trim().Length)
                {
                    throw new Exception("Date comparison failed");
                }
                else
                {
                    Int64 pcValue = Convert.ToInt64(pcStamp);
                    Int64 qlValue = Convert.ToInt64(savedStamp);

                    LicenseValidator.pcValue = pcValue.ToString();
                    LicenseValidator.pcTimeStamp = pcStamp;

                    LicenseValidator.SavedValue = qlValue.ToString();
                    LicenseValidator.SavedTimeStamp = savedStamp;


                    if (pcValue >= qlValue)
                    {
                        isPCDateValid = true;
                    }
                    else
                    {
                        isPCDateValid = false;
                    }
                }
            }
            catch (Exception ex)
            {

            }



            return isPCDateValid;
        }

        private static string GetSecureAppTimeStamp()
        {
            string storedStamp = "";
            storedStamp = GetSavedStamp();

            return storedStamp;
        }
        #region R/W Regsitry

        /// <summary>
        /// Saves the current TimeStamp from Internet if connected else PC date
        /// </summary>
        public static void SavePresentTimeStamp()
        {
            bool isSaved = false;
            string timeStampToSave = DateTime.Now.ToString("yyyy MM dd HH:mm:ss").Replace(" ", "").Replace(":", "");
            if (Utility.IsInternetConnected())
            {
                timeStampToSave = GetTimeStampFromInternet();
            }

            if(!IsPCDateValidWithoutNet())
            {
                return;
            }

            timeStampToSave = Utility.ConvertToEncrypt(timeStampToSave);
            try
            {

                #region FirstStore-Reg
                RegistryKey key = Registry.CurrentUser.CreateSubKey(@"SOFTWARE\WinRegistry");
                key.SetValue("MicrosoftProVersion", timeStampToSave);
                isSaved = true;
                try
                {
                    key.Close();
                }
                catch
                { }
            }
            catch (Exception exp1)
            {

            }
            #endregion
            //-------------------------------------------------------------------


            #region SecondStore-Isolated Storage
            try
            {
                IsolatedStorageFile isoStore = IsolatedStorageFile.GetStore(IsolatedStorageScope.User | IsolatedStorageScope.Assembly, null, null);

                if (isoStore.FileExists("MicrosoftProVersion"))
                {                    
                    isoStore.DeleteDirectory("MicrosoftProVersion");
                }
                //else
                {
                    using (IsolatedStorageFileStream isoStream = new IsolatedStorageFileStream("MicrosoftProVersion", FileMode.CreateNew, isoStore))
                    {
                        using (StreamWriter writer = new StreamWriter(isoStream))
                        {
                            writer.WriteLine(timeStampToSave);                            
                            isSaved = true;
                        }
                    }
                }
            }
            catch (Exception exp2)
            {

            }
            #endregion
            //------------------------------------------------------------------

            #region ThirdStore-Local File
            try
            {
                //string filepath= Environment.GetFolderPath(Environment.SpecialFolder.AppData)+@"/"
                string filepath = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + @"/MicrosoftProVersion";
                File.WriteAllText(filepath, timeStampToSave);
                isSaved = true;
            }
            catch (Exception exp3)
            {

            }

            #endregion

            if(isSaved==false)
            {
                throw new Exception("Stamp couldnot be saved");
            }

            #region ThirdStore-Environment Variable

            ////var value = System.Environment.GetEnvironmentVariable(variable[, Target])
            //System.Environment.SetEnvironmentVariable(("MicrosoftProVersion", timeStampToSave.ToString(), EnvironmentVariableTarget.Machine);
            #endregion

        }

        private static string GetSavedStamp()
        {
            string encryptedStamp = "";
            string decryptedStamp = "";
            #region FirstStore - WinRig
            try
            {
                RegistryKey key = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\WinRegistry");
                if (key != null)
                {
                    encryptedStamp =((key.GetValue("MicrosoftProVersion", "").ToString()));

                    try
                    {
                        key.Close();
                    }
                    catch (Exception ex)
                    {
                    }
                    decryptedStamp = Utility.ConvertToStringFromEncrypt(encryptedStamp);
                    if (IsStampValid(decryptedStamp))
                    {
                        return decryptedStamp;
                    }
                }
            }
            catch { }
            finally { }
            #endregion
            //-------------------------------------
            #region SecondStore - Isolated Storage
            try
            {
                IsolatedStorageFile isoStore = IsolatedStorageFile.GetStore(IsolatedStorageScope.User | IsolatedStorageScope.Assembly, null, null);

                if (isoStore.FileExists("MicrosoftProVersion"))
                {
                    //Console.WriteLine("The file already exists!");
                    using (IsolatedStorageFileStream isoStream = new IsolatedStorageFileStream("MicrosoftProVersion", FileMode.Open, isoStore))
                    {
                        using (StreamReader reader = new StreamReader(isoStream))
                        {
                            //Console.WriteLine("Reading contents:");
                            //Console.WriteLine(reader.ReadToEnd());
                            encryptedStamp = reader.ReadToEnd();
                            isoStore.Close();
                        }
                    }

                }
                if(encryptedStamp!=null && encryptedStamp!="")
                {
                    decryptedStamp = Utility.ConvertToStringFromEncrypt(encryptedStamp);
                    if (IsStampValid(decryptedStamp))
                        return decryptedStamp;
                }
                
            }
            catch (Exception exp2)
            {
                System.Windows.Forms.MessageBox.Show(exp2.Message + Environment.NewLine + exp2.StackTrace);
            }
            #endregion
            //-------------------------------------
            #region ThirdStore - File
            try
            {
                string filepath = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + @"/MicrosoftProVersion";
                if (File.Exists(filepath))
                {
                    encryptedStamp = File.ReadAllText(filepath);
                    if (encryptedStamp != null && encryptedStamp != "")
                    {
                        decryptedStamp = Utility.ConvertToStringFromEncrypt(encryptedStamp);
                        if (IsStampValid(decryptedStamp))
                            return decryptedStamp;
                    }
                }
            }
            catch(Exception exfile)
            {

            }
            #endregion
            if (!IsStampValid(decryptedStamp))
            {
                decryptedStamp = (Convert.ToInt64(DateTime.Now.ToString("yyyy MM dd HH:mm:ss").Replace(" ", "").Replace(":", "")) - 10).ToString();
            }

                return decryptedStamp;
        }

        private static bool IsStampValid(string stamp)
        {
            bool isValid = false;
            if (stamp.Trim() != null && stamp.Trim().Length == 14)
                isValid = true;

            return isValid;
        }
        #endregion
    }
}
