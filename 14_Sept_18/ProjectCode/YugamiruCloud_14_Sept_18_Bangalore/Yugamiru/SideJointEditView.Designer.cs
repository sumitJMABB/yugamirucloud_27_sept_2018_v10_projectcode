﻿namespace Yugamiru
{
    partial class SideJointEditView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.IDC_Mag1Btn = new System.Windows.Forms.PictureBox();
            this.IDC_Mag2Btn = new System.Windows.Forms.PictureBox();
            this.IDC_Slider1 = new System.Windows.Forms.TrackBar();
            this.IDC_ResetImgBtn = new System.Windows.Forms.PictureBox();
            this.IDC_OkBtn = new System.Windows.Forms.PictureBox();
            this.IDC_CancelBtn = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_Mag1Btn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_Mag2Btn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_Slider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_ResetImgBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_OkBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_CancelBtn)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(68, 62);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(105, 46);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox1_Paint);
            this.pictureBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDown);
            this.pictureBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseMove);
            this.pictureBox1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseUp);
            // 
            // IDC_Mag1Btn
            // 
            this.IDC_Mag1Btn.Location = new System.Drawing.Point(318, 83);
            this.IDC_Mag1Btn.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.IDC_Mag1Btn.Name = "IDC_Mag1Btn";
            this.IDC_Mag1Btn.Size = new System.Drawing.Size(50, 25);
            this.IDC_Mag1Btn.TabIndex = 1;
            this.IDC_Mag1Btn.TabStop = false;
            this.IDC_Mag1Btn.Click += new System.EventHandler(this.IDC_Mag1Btn_Click);
            // 
            // IDC_Mag2Btn
            // 
            this.IDC_Mag2Btn.Location = new System.Drawing.Point(318, 286);
            this.IDC_Mag2Btn.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.IDC_Mag2Btn.Name = "IDC_Mag2Btn";
            this.IDC_Mag2Btn.Size = new System.Drawing.Size(50, 25);
            this.IDC_Mag2Btn.TabIndex = 2;
            this.IDC_Mag2Btn.TabStop = false;
            this.IDC_Mag2Btn.Click += new System.EventHandler(this.IDC_Mag2Btn_Click);
            // 
            // IDC_Slider1
            // 
            this.IDC_Slider1.Location = new System.Drawing.Point(0, -3);
            this.IDC_Slider1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.IDC_Slider1.Name = "IDC_Slider1";
            this.IDC_Slider1.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.IDC_Slider1.Size = new System.Drawing.Size(69, 200);
            this.IDC_Slider1.TabIndex = 3;
            this.IDC_Slider1.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.IDC_Slider1.Scroll += new System.EventHandler(this.IDC_Slider1_Scroll);
            // 
            // IDC_ResetImgBtn
            // 
            this.IDC_ResetImgBtn.Location = new System.Drawing.Point(318, 337);
            this.IDC_ResetImgBtn.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.IDC_ResetImgBtn.Name = "IDC_ResetImgBtn";
            this.IDC_ResetImgBtn.Size = new System.Drawing.Size(50, 46);
            this.IDC_ResetImgBtn.TabIndex = 4;
            this.IDC_ResetImgBtn.TabStop = false;
            this.IDC_ResetImgBtn.Click += new System.EventHandler(this.IDC_ResetImgBtn_Click);
            // 
            // IDC_OkBtn
            // 
            this.IDC_OkBtn.Location = new System.Drawing.Point(58, 303);
            this.IDC_OkBtn.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.IDC_OkBtn.Name = "IDC_OkBtn";
            this.IDC_OkBtn.Size = new System.Drawing.Size(51, 48);
            this.IDC_OkBtn.TabIndex = 5;
            this.IDC_OkBtn.TabStop = false;
            this.IDC_OkBtn.Click += new System.EventHandler(this.IDC_OkBtn_Click);
            // 
            // IDC_CancelBtn
            // 
            this.IDC_CancelBtn.Location = new System.Drawing.Point(176, 303);
            this.IDC_CancelBtn.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.IDC_CancelBtn.Name = "IDC_CancelBtn";
            this.IDC_CancelBtn.Size = new System.Drawing.Size(51, 48);
            this.IDC_CancelBtn.TabIndex = 6;
            this.IDC_CancelBtn.TabStop = false;
            this.IDC_CancelBtn.Click += new System.EventHandler(this.IDC_CancelBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(46, 117);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 20);
            this.label1.TabIndex = 7;
            this.label1.Text = "label1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(46, 154);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 20);
            this.label2.TabIndex = 8;
            this.label2.Text = "label2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Lime;
            this.label3.Location = new System.Drawing.Point(46, 189);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 26);
            this.label3.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Lime;
            this.label4.Location = new System.Drawing.Point(46, 229);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 26);
            this.label4.TabIndex = 10;
            this.label4.Text = "cm";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(159, 154);
            this.textBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(148, 26);
            this.textBox1.TabIndex = 11;
            this.textBox1.Text = "80";
            this.textBox1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox1_KeyPress);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.IDC_Slider1);
            this.panel1.Location = new System.Drawing.Point(459, 62);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(76, 213);
            this.panel1.TabIndex = 12;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // SideJointEditView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(636, 402);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.IDC_CancelBtn);
            this.Controls.Add(this.IDC_OkBtn);
            this.Controls.Add(this.IDC_ResetImgBtn);
            this.Controls.Add(this.IDC_Mag2Btn);
            this.Controls.Add(this.IDC_Mag1Btn);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "SideJointEditView";
            this.Text = "SideJointEditView";
            this.Load += new System.EventHandler(this.SideJointEditView_Load);
            this.SizeChanged += new System.EventHandler(this.SideJointEditView_SizeChanged);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.SideJointEditView_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_Mag1Btn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_Mag2Btn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_Slider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_ResetImgBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_OkBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_CancelBtn)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox IDC_Mag1Btn;
        private System.Windows.Forms.PictureBox IDC_Mag2Btn;
        private System.Windows.Forms.TrackBar IDC_Slider1;
        private System.Windows.Forms.PictureBox IDC_ResetImgBtn;
        private System.Windows.Forms.PictureBox IDC_OkBtn;
        private System.Windows.Forms.PictureBox IDC_CancelBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Timer timer1;
    }
}